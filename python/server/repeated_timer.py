"""A module containing a simple repeating timer class"""
import time
import threading as thr
class RepeatedTimer(object):
    """A simple repeating timer class"""
    def __init__(self, interval, function, *args, **kwargs):
        """Initialises the RepeatedTimer instance"""
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.next_call = time.time()
        self.start()
    def _run(self):
        """Runs timer"""
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)
    def start(self):
        """Starts timer"""
        if not self.is_running:
            self.next_call += self.interval
            self._timer = thr.Timer(self.next_call - time.time(), self._run)
            self._timer.start()
            self.is_running = True
    def stop(self):
        """Stops timer"""
        self._timer.cancel()
        self.is_running = False
