"""Collection of API commands available when communicating with XPS-C8."""
class XPSC8_API:
    """Collection of API commands available when communicating with XPS-C8."""

    def __init__(self, s):
        self.s = s


    def send(self, commandString):
        """Sends the command string to the XPS-C8."""
        self.s.send(commandString.encode())


    def CloseAllOtherSockets(self):
        """Function argument(s) : CloseAllOtherSockets
        Close all socket beside the one used to send this command
        Uses no parameters
        Example Command Execution:
            "CloseAllOtherSockets()"
        Example Expected Response:
            -107,CloseAllOtherSockets()
        """
        self.send("CloseAllOtherSockets()")


    def ControllerMotionKernelTimeLoadGet(self):
        """Function argument(s) : ControllerMotionKernelTimeLoadGet
        Get controller motion kernel time load
        double *CPUTotalLoadRatio

        double *CPUTotalLoadRatio
        double *CPUCorrectorLoadRatio

        double *CPUCorrectorLoadRatio
        double *CPUProfilerLoadRatio

        double *CPUProfilerLoadRatio
        double *CPUServitudesLoadRatio

        double *CPUServitudesLoadRatio

        Example Command Execution:
            "ControllerMotionKernelTimeLoadGet(double  *,double  *,double  *,double  *)"
        Example Expected Response:
            ControllerMotionKernelTimeLoadGet(double *,double *,double *,double *)0,0.3999735126854,0.360442645615,0.003480854502413,0.03605001256801
        """
        self.send("ControllerMotionKernelTimeLoadGet(double  *,double  *,double  *,double  *)")


    def ControllerStatusGet(self):
        """Function argument(s) : ControllerStatusGet
        Read controller current status
        int *ControllerStatus

        Example Command Execution:
            ControllerStatusGet(int  *)
        Example Expected Response:
            0,0
        """
        self.send("ControllerStatusGet(int  *)")


    def ControllerStatusStringGet(self, ControllerStatusCode: int):
        """Function argument(s) : ControllerStatusStringGet
        Return the controller status string corresponding to the controller status code
        int ControllerStatusCode

        0
        char ControllerStatusString[]

        char ControllerStatusString[]

        Example Command Execution:
            ControllerStatusStringGet(0,char *)
        Example Expected Response:
            0,Controller status OK
        """
        self.send("ControllerStatusStringGet("+str(ControllerStatusCode)+",char *)")


    def DoubleGlobalArrayGet(self, Number: int):
        """Function argument(s) : DoubleGlobalArrayGet
        Get double global array value
        int Number

        0
        double* DoubleValue

        double* DoubleValue

        Example Command Execution:
            DoubleGlobalArrayGet(0,double *)
        Example Expected Response:
            0,0
        """
        self.send("DoubleGlobalArrayGet("+str(Number)+",double *)")


    def DoubleGlobalArraySet(self, Number: int, DoubleValue: float):
        """Function argument(s) : DoubleGlobalArraySet
        Set double global array value
        int Number

        0
        double DoubleValue


        Example Command Execution:
            DoubleGlobalArraySet(0,0)
        Example Expected Response:
            0,
        """
        self.send("DoubleGlobalArraySet("+str(Number)+","+str(DoubleValue)+")")


    def ElapsedTimeGet(self):
        """Function argument(s) : ElapsedTimeGet
        Return elapsed time from controller power on
        double* ElapsedTime

        double* ElapsedTime


        Example Command Execution:
            ElapsedTimeGet(double *)
        Example Expected Response:
            0,85285.682328
        """
        self.send("ElapsedTimeGet(double *)")


    def ErrorStringGet(self, ErrorStringGet: int):
        """Function argument(s) : ErrorStringGet
        Return the error string corresponding to the error code
        int ErrorCode

        0
        char ErrorString[]

        char ErrorString[]

        Example Command Execution:
            ErrorStringGet(0,char *)
        Example Expected Response:
            0,Successful command
        """
        self.send("ErrorStringGet("+str(ErrorStringGet)+",char *)")


    def EventExtendedAllGet(self):
        """Function argument(s) : EventExtendedAllGet
        Read all event and action configurations
        char EventActionConfigurations[]

        char EventActionConfigurations[]

        Example Command Execution:
            EventExtendedAllGet(char *)
        Example Expected Response:
            -83,EventExtendedAllGet(char *)
        """
        self.send("EventExtendedAllGet(char *)")


    def EventExtendedConfigurationActionGet(self):
        """Function argument(s) : EventExtendedConfigurationActionGet
        Read the action configuration
        char ActionConfiguration[]

        char ActionConfiguration[]


        Example Command Execution:
            EventExtendedConfigurationActionGet(char *)
        Example Expected Response:
            -81,EventExtendedConfigurationActionGet(char *)
        """
        self.send("EventExtendedConfigurationActionGet(char *)")


    def EventExtendedConfigurationActionSet(
            self, ExtendedActionName: str, ActionParameter1: str,
            ActionParameter2: str, ActionParameter3: str,
            ActionParameter4: str):
        """Function argument(s) : EventExtendedConfigurationActionSet
        Configure one or several actions
        char ExtendedActionName[251]
            Mulitple options are possible see ExtendedActionNameList below.
        char ActionParameter1[251]

        0
        char ActionParameter2[251]

        0
        char ActionParameter3[251]

        0
        char ActionParameter4[251]

        0


        Example Command Execution:
            EventExtendedConfigurationActionSet(GROUP6,0,0,0,0)
        Example Expected Responses:
            -39,EventExtendedConfigurationActionSet(GROUP6,0,0,0,0)
            -39,EventExtendedConfigurationActionSet(0,0,0,0,0)
        """

        # ExtendedActionNameList = [
        #     "GROUP1",
        #     "GROUP1.POSITIONER",
        #     "GROUP2",
        #     "GROUP2.POSITIONER",
        #     "GROUP3",
        #     "GROUP3.POSITIONER",
        #     "GROUP4",
        #     "GROUP4.POSITIONER",
        #     "GROUP5",
        #     "GROUP5.POSITIONER",
        #     "GROUP6",
        #     "GROUP6.POSITIONER",
        #     "GPIO2.ADC1",
        #     "GPIO2.ADC2",
        #     "GPIO2.ADC3",
        #     "GPIO2.ADC4",
        #     "GPIO2.DAC1",
        #     "GPIO2.DAC2",
        #     "GPIO2.DAC3",
        #     "GPIO2.DAC4",
        #     "GPIO1.DI",
        #     "GPIO2.DI",
        #     "GPIO3.DI",
        #     "GPIO4.DI",
        #     "GPIO1.DO",
        #     "GPIO3.DO",
        #     "GPIO4.DO",
        #     "DACSet.SetpointPosition",
        #     "DACSet.SetpointVelocity",
        #     "DACSet.SetpointAcceleration",
        #     "DACSet.CurrentPosition",
        #     "DACSet.CurrentVelocity",
        #     "DACSet.Value",
        #     "DOPulse",
        #     "DOSet",
        #     "DOToggle",
        #     "MoveAbort",
        #     "GatheringRun",
        #     "GatheringOneData",
        #     "GatheringRunAppend",
        #     "GatheringStop",
        #     "ExternalGatheringRun",
        #     "ExecuteTCLScript",
        #     "KillTCLScript"
        # ]
        self.send("EventExtendedConfigurationActionSet("+
                  ExtendedActionName+","+
                  ActionParameter1+","+
                  ActionParameter2+","+
                  ActionParameter3+","+
                  ActionParameter4+
                  ")")


    def EventExtendedConfigurationTriggerGet(self):
        """Function argument(s) : EventExtendedConfigurationTriggerGet
        Read the event configuration
        char EventTriggerConfiguration[]

        char EventTriggerConfiguration[]

        Example Command Execution:
            EventExtendedConfigurationTriggerGet(char *)
        Example Expected Response:
            -80,EventExtendedConfigurationTriggerGet(char *)
        """
        self.send("EventExtendedConfigurationTriggerGet(char *)")


    def EventExtendedConfigurationTriggerSet(
            self, ExtendedEventName: str, EventParameter1: str,
            EventParameter2: str, EventParameter3: str, EventParameter4: str):
        """Function argument(s) : EventExtendedConfigurationTriggerSet
        Configure one or several events
        char ExtendedEventName[251]
            "GROUP1",
            "GROUP1.POSITIONER",
            "GROUP2",
            "GROUP2.POSITIONER",
            "GROUP3",
            "GROUP3.POSITIONER",
            "GROUP4",
            "GROUP4.POSITIONER",
            "GROUP5",
            "GROUP5.POSITIONER",
            "GROUP6",
            "GROUP6.POSITIONER",
            "GPIO2.ADC1",
            "GPIO2.ADC2",
            "GPIO2.ADC3",
            "GPIO2.ADC4",
            "GPIO2.DAC1",
            "GPIO2.DAC2",
            "GPIO2.DAC3",
            "GPIO2.DAC4",
            "GPIO1.DI",
            "GPIO2.DI",
            "GPIO3.DI",
            "GPIO4.DI",
            "GPIO1.DO",
            "GPIO3.DO",
            "GPIO4.DO",
            "Immediate",
            "Always",
            "Timer1",
            "Timer2",
            "Timer3",
            "Timer4",
            "Timer5",
            "ADCHighLimit",
            "ADCLowLimit",
            "DIHighLow",
            "DILowHigh",
            "DIToggled",
            "PositionerError",
            "PositionerHardwareStatus",
            "MotionDone",
            "SGamma.ConstantVelocityStart",
            "SGamma.ConstantVelocityEnd",
            "SGamma.ConstantVelocityState",
            "SGamma.ConstantAccelerationStart",
            "SGamma.ConstantAccelerationEnd",
            "SGamma.ConstantAccelerationState",
            "SGamma.ConstantDecelerationStart",
            "SGamma.ConstantDecelerationEnd",
            "SGamma.ConstantDecelerationState",
            "SGamma.MotionStart",
            "SGamma.MotionEnd",
            "SGamma.MotionState",
            "Jog.ConstantVelocityStart",
            "Jog.ConstantVelocityEnd",
            "Jog.ConstantVelocityState",
            "Jog.ConstantAccelerationStart",
            "Jog.ConstantAccelerationEnd",
            "Jog.ConstantAccelerationState",
            "Jog.MotionStart",
            "Jog.MotionEnd",
            "Jog.MotionState",
            "Spin.ConstantVelocityStart",
            "Spin.ConstantVelocityEnd",
            "Spin.ConstantVelocityState",
            "Spin.ConstantAccelerationStart",
            "Spin.ConstantAccelerationEnd",
            "Spin.ConstantAccelerationState",
            "Spin.MotionStart",
            "Spin.MotionEnd",
            "Spin.MotionState",
            "XYLineArc.TrajectoryStart",
            "XYLineArc.TrajectoryEnd",
            "XYLineArc.TrajectoryState",
            "XYLineArc.ElementNumberStart",
            "XYLineArc.ElementNumberState",
            "XYLineArc.TrajectoryPulseState",
            "XYLineArc.TrajectoryPulse",
            "Spline.TrajectoryStart",
            "Spline.TrajectoryEnd",
            "Spline.TrajectoryState",
            "Spline.ElementNumberStart",
            "Spline.ElementNumberState",
            "Spline.TrajectoryPulseState",
            "Spline.TrajectoryPulse",
            "PVT.TrajectoryStart",
            "PVT.TrajectoryEnd",
            "PVT.TrajectoryState",
            "PVT.ElementNumberStart",
            "PVT.ElementNumberState",
            "PVT.TrajectoryPulseState",
            "PVT.TrajectoryPulse"
        char EventParameter1[251]

        0
        char EventParameter2[251]

        0
        char EventParameter3[251]

        0
        char EventParameter4[251]

        0


        Example Command Execution:
            EventExtendedConfigurationTriggerSet(0,0,0,0,0)
        Example Expected Response:
            -40,EventExtendedConfigurationTriggerSet(char ExtendedEventName[251],0,0,0,0)
        """
        self.send("EventExtendedConfigurationTriggerSet("+ExtendedEventName+","+
                  EventParameter1+","+EventParameter2+","+EventParameter3+","+
                  EventParameter4+
                  ")")


    def EventExtendedGet(self, ID: int):
        """Function argument(s) : EventExtendedGet
        Read the event and action configuration defined by ID
        int ID

        0
        char EventTriggerConfiguration[]

        char EventTriggerConfiguration[]
        char ActionConfiguration[]

        char ActionConfiguration[]


        Example Command Execution:
            EventExtendedGet(0,char *,char *)
        Example Expected Response:
            -83,EventExtendedGet(0,char *,char *)
        """
        self.send("EventExtendedGet("+str(ID)+",char *,char *)")


    def EventExtendedRemove(self, ID: int):
        """Function argument(s) : EventExtendedRemove
        Remove the event and action configuration defined by ID
        int ID

        0


        Example Command Execution:
            EventExtendedRemove(0)
        Example Expected Response:
            0,
        """
        self.send("EventExtendedRemove("+str(ID)+")")


    def EventExtendedStart(self):
        """Function argument(s) : EventExtendedStart
        Launch the last event and action configuration and return an ID
        int* ID

        int* ID

        Example Command Execution:
            EventExtendedStart(int *)
        Example Expected Response:
            -80,EventExtendedStart(int *)
        """
        self.send("EventExtendedStart(int *)")


    def EventExtendedWait(self):
        """Function argument(s) : EventExtendedWait
        Wait events from the last event configuration

        Example Command Execution:
            EventExtendedWait()
        Example Expected Response:
            -80,EventExtendedWait()
        """
        self.send("EventExtendedWait()")


    def FirmwareVersionGet(self):
        """Function argument(s) : FirmwareVersionGet
        Return firmware version
        char Version[]

        char Version[]


        Example Command Execution:
            FirmwareVersionGet(char *)
        Example Expected Response:
            0,XPS-C8 Firmware V2.7.0
        """
        self.send("FirmwareVersionGet(char *)")


    def GatheringConfigurationGet(self):
        """Function argument(s) : GatheringConfigurationGet
        Read different mnemonique type
        char Type[]

        char Type[]

        Example Command Execution:
            GatheringConfigurationGet(char *)
        Example Expected Response:
            -32,GatheringConfigurationGet(char *)
        """
        self.send("GatheringConfigurationGet(char *)")


    def GatheringConfigurationSet(self, Type: str):
        """Function argument(s) : GatheringConfigurationSet
        Configuration acquisition
        char Type[251]
            "GROUP1.POSITIONER"
            "GROUP2.POSITIONER"
            "GROUP3.POSITIONER"
            "GROUP4.POSITIONER"
            "GROUP5.POSITIONER"
            "GROUP6.POSITIONER"
            "SetpointPosition"
            "CurrentPosition"
            "FollowingError"
            "SetpointVelocity"
            "SetpointAcceleration"
            "CurrentVelocity"
            "CurrentAcceleration"
            "CorrectorOutput"
            "ExcitationSignalInput"
            "GPIO1.DI"
            "GPIO1.DO"
            "GPIO2.DI"
            "GPIO3.DI"
            "GPIO3.DO"
            "GPIO4.DI"
            "GPIO4.DO"
            "GPIO2.ADC1"
            "GPIO2.ADC2"
            "GPIO2.ADC3"
            "GPIO2.ADC4"
            "GPIO2.DAC1"
            "GPIO2.DAC2"
            "GPIO2.DAC3"
            "GPIO2.DAC4"
            "CPUTotalLoadRatio"

        Example Command Execution:
            GatheringConfigurationSet(CPUTotalLoadRatio)
        Example Expected Response:
            -29,GatheringConfigurationSet(GROUP1.POSITIONER)
        """
        self.send("GatheringConfigurationSet("+Type+")")


    def GatheringCurrentNumberGet(self):
        """Function argument(s) : GatheringCurrentNumberGet
        Maximum number of samples and current number during acquisition
        int* CurrentNumber

        int* CurrentNumber
        int* MaximumSamplesNumber

        int* MaximumSamplesNumber


        Example Command Execution:
            GatheringCurrentNumberGet(int *,int *)
        Example Expected Response:
            -32,GatheringCurrentNumberGet(int *,int *)
        """
        self.send("GatheringCurrentNumberGet(int *,int *)")


    def GatheringDataAcquire(self):
        """Function argument(s) : GatheringDataAcquire
        Acquire a configured data

        Example Command Execution:
            GatheringDataAcquire()
        Example Expected Response:
            -32,GatheringDataAcquire()
        """
        self.send("GatheringDataAcquire()")


    def GatheringDataGet(self, IndexPoint: int):
        """Function argument(s) : GatheringDataGet
        Get a data line from gathering buffer
        int IndexPoint

        0
        char DataBufferLine[]

        char DataBufferLine[]


        Example Command Execution:
            GatheringDataGet(0,char *)
        Example Expected Response:
            -32,GatheringDataGet(0,char *)
        """
        self.send("GatheringDataGet("+str(IndexPoint)+",char *)")


    def GatheringDataMultipleLinesGet(self, IndexPoint: int, NumberOfLines: int):
        """Function argument(s) : GatheringDataMultipleLinesGet
        Get multiple data lines from gathering buffer
        int IndexPoint

        0
        int NumberOfLines

        0
        char DataBufferLine[]

        char DataBufferLine[]


        Example Command Execution:
            GatheringDataMultipleLinesGet(0,0,char *)
        Example Expected Response:
            -32,GatheringDataMultipleLinesGet(0,0,char *)
        """
        self.send("GatheringDataMultipleLinesGet("+str(IndexPoint)+","+
                  str(NumberOfLines)+",char *)")


    def GatheringExternalConfigurationGet(self):
        """Function argument(s) : GatheringExternalConfigurationGet
        Read different mnemonique type
        char Type[]

        char Type[]


        Example Command Execution:
            GatheringExternalConfigurationGet(char *)
        Example Expected Response:
            -32,GatheringExternalConfigurationGet(char *)
        """
        self.send("GatheringExternalConfigurationGet(char *)")


    def GatheringExternalConfigurationSet(self, Type: str):
        """Function argument(s) : GatheringExternalConfigurationSet
        Configuration acquisition
        char Type[251]
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
            ExternalLatchPosition
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.ADC5
            GPIO2.ADC6
            GPIO2.ADC7
            GPIO2.ADC8

        Example Command Execution:
            GatheringExternalConfigurationSet(GROUP1.POSITIONER)
        Example Expected Response:
            -29,GatheringExternalConfigurationSet(GROUP1.POSITIONER)
        """
        self.send("GatheringExternalConfigurationSet("+Type+")")


    def GatheringExternalCurrentNumberGet(self):
        """Function argument(s) : GatheringExternalCurrentNumberGet
        Maximum number of samples and current number during acquisition
        int* CurrentNumber

        int* CurrentNumber
        int* MaximumSamplesNumber

        int* MaximumSamplesNumber


        Example Command Execution:
            GatheringExternalCurrentNumberGet(int *,int *)
        Example Expected Response:
            -32,GatheringExternalCurrentNumberGet(int *,int *)
        """
        self.send("GatheringExternalCurrentNumberGet(int *,int *)")


    def GatheringExternalDataGet(self, IndexPoint: int):
        """Function argument(s) : GatheringExternalDataGet
        Get a data line from external gathering buffer
        int IndexPoint

        0
        char DataBufferLine[]

        char DataBufferLine[]


        Example Command Execution:
            GatheringExternalDataGet(0,char *)
        Example Expected Response:
            -32,GatheringExternalDataGet(0,char *)
        """
        self.send("GatheringExternalDataGet("+str(IndexPoint)+",char *)")


    def GatheringExternalStopAndSave(self):
        """Function argument(s) : GatheringExternalStopAndSave
        Stop acquisition and save data

        Example Command Execution:
            GatheringExternalStopAndSave()
        Example Expected Response:
            -30,GatheringExternalStopAndSave()
        """
        self.send("GatheringExternalStopAndSave()")


    def GatheringReset(self):
        """Function argument(s) : GatheringReset
        Empty the gathered data in memory to start new gathering from scratch

        Example Command Execution:
            GatheringReset()
        Example Expected Response:
            0,
        """
        self.send("GatheringReset()")


    def GatheringRunAppend(self):
        """Function argument(s) : GatheringRunAppend
        Re-start the stopped gathering to add new data

        Example Command Execution:
            GatheringRunAppend()
        Example Expected Response:
            -32,GatheringRunAppend()
        """
        self.send("GatheringRunAppend()")


    def GatheringRun(self, DataNumber: int, Divisor: int):
        """Function argument(s) : GatheringRun
        Start a new gathering
        int DataNumber

        0
        int Divisor

        0


        Example Command Execution:
            GatheringRun(0,0)
        Example Expected Response:
            -32,GatheringRun(0,0)
        """
        self.send("GatheringRun("+str(DataNumber)+","+str(Divisor)+")")


    def GatheringStop(self):
        """Function argument(s) : GatheringStop
        Stop the data gathering (without saving to file)

        Example Command Execution:
            GatheringStop()
        Example Expected Response:
            0,
        """
        self.send("GatheringStop()")


    def GatheringStopAndSave(self):
        """Function argument(s) : GatheringStopAndSave
        Stop acquisition and save data

        Example Command Execution:
            GatheringStopAndSave()
        Example Expected Response:
            -30,GatheringStopAndSave()
        """
        self.send("GatheringStopAndSave()")


    def GlobalArrayGet(self, Number: int):
        """Function argument(s) : GlobalArrayGet
        Get global array value
        int Number

        0
        char ValueString[]

        char ValueString[]


        Example Command Execution:
            GlobalArrayGet(0,char *)
        Example Expected Response:
            0,
        """
        self.send("GlobalArrayGet("+str(Number)+",char *)")


    def GlobalArraySet(self, Number: int, ValueString: str):
        """Function argument(s) : GlobalArraySet
        Set global array value
        int Number

        0
        char ValueString[500]

        0


        Example Command Execution:
            GlobalArraySet(0,0)
        Example Expected Response:
            0,
        """
        self.send("GlobalArraySet("+str(Number)+","+ValueString+")")


    def GPIOAnalogGainGet(self, GPIOName: str):
        """Function argument(s) : GPIOAnalogGainGet
        Read analog input gain (1, 2, 4 or 8) for one or few input
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        int* AnalogInputGainValue

        int* AnalogInputGainValue


        Example Command Execution:
            GPIOAnalogGainGet(GPIO2.ADC1,int *)
        Example Expected Response:
            0,1
        """
        self.send("GPIOAnalogGainGet("+GPIOName+",int *)")


    def GPIOAnalogGainSet(self, GPIOName: str, AnalogInputGainValue: int):
        """Function argument(s) : GPIOAnalogGainSet
        Set analog input gain (1, 2, 4 or 8) for one or few input
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        int AnalogInputGainValue

        0


        Example Command Execution:
            GPIOAnalogGainSet(GPIO2.ADC1,1)
        Example Expected Response:
            0,
        """
        self.send("GPIOAnalogGainSet("+GPIOName+","+str(AnalogInputGainValue)+")")


    def GPIOAnalogGet(self, GPIOName: str):
        """Function argument(s) : GPIOAnalogGet
        Read analog input or analog output for one or few input
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        double* AnalogValue

        double* AnalogValue


        Example Command Execution:
            GPIOAnalogGet(GPIO2.ADC1,double *)
        Example Expected Response:
            0,0.00458754998755
        """
        self.send("GPIOAnalogGet("+GPIOName+",double *)")


    def GPIOAnalogSet(self, GPIOName: str, AnalogOutputValue: float):
        """Function argument(s) : GPIOAnalogSet
        Set analog output for one or few output
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        double AnalogOutputValue

        0


        Example Command Execution:
            GPIOAnalogSet(GPIO2.ADC1,0.00458754998755)
        Example Expected Response:
            -8,GPIOAnalogSet(GPIO2.ADC1,0.00458754998755)
        """
        self.send("GPIOAnalogSet("+GPIOName+","+str(AnalogOutputValue)+")")


    def GPIODigitalGet(self, GPIOName: str):
        """Function argument(s) : GPIODigitalGet
        Read digital output or digital input
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        unsigned short* DigitalValue

        unsigned short* DigitalValue


        Example Command Execution:
            GPIODigitalGet(GPIO2.ADC1,unsigned short *)
        Example Expected Response:
            -8,GPIODigitalGet(GPIO2.ADC1,unsigned short *)
        """
        self.send("GPIODigitalGet("+GPIOName+",unsigned short *)")


    def GPIODigitalSet(self, GPIOName: str, Mask: int, DigitalOutputValue: int):
        """Function argument(s) : GPIODigitalSet
        Set Digital Output for one or few output TTL
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        unsigned short Mask

        0
        unsigned short DigitalOutputValue

        0

        Example Command Execution:
            GPIODigitalSet(GPIO2.ADC1,0,0)
        Example Expected Response:
            ?
        """
        self.send("GPIODigitalSet("+GPIOName+","+str(Mask)+","+str(DigitalOutputValue)+")")


    def GroupAccelerationSetpointGet(self, GroupName: str):
        """Function argument(s) : GroupAccelerationSetpointGet
        Return setpoint accelerations
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *SetpointAcceleration

        double *SetpointAcceleration

        Example Command Execution:
            GroupAccelerationSetpointGet(GROUP1,double  *)
        Example Expected Response:
            0,0
        """
        self.send("GroupAccelerationSetpointGet("+GroupName+",double  *)")


    def GroupAnalogTrackingModeDisable(self, GroupName: str):
        """Function argument(s) : GroupAnalogTrackingModeDisable
        Disable Analog Tracking mode on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            GroupAnalogTrackingModeDisable(GROUP1)
        Example Expected Response:
            -22,GroupAnalogTrackingModeDisable(GROUP1)
        """
        self.send("GroupAnalogTrackingModeDisable("+GroupName+")")


    def GroupAnalogTrackingModeEnable(self, GroupName: str, Type: str):
        """Function argument(s) : GroupAnalogTrackingModeEnable
        Enable Analog Tracking mode on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char Type[251]
            Position
            Velocity


        Example Command Execution:
            GroupAnalogTrackingModeEnable(GROUP1,Position)
        Example Expected Response:
            ?
        """
        self.send("GroupAnalogTrackingModeEnable("+GroupName+","+Type+")")


    def GroupCorrectorOutputGet(self, GroupName: str):
        """Function argument(s) : GroupCorrectorOutputGet
        Return corrector outputs
        char GroupName[251]

        double *CorrectorOutput
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *CorrectorOutput


        Example Command Execution:
            GroupCorrectorOutputGet(GROUP1,double  *)
        Example Expected Response:
            0,0
        """
        self.send("GroupCorrectorOutputGet("+GroupName+",double  *)")


    def GroupCurrentFollowingErrorGet(self, GroupName: str):
        """Function argument(s) : GroupCurrentFollowingErrorGet
        Return current following errors
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *CurrentFollowingError

        double *CurrentFollowingError


        Example Command Execution:
            GroupCurrentFollowingErrorGet(GROUP1,double  *)
        Example Expected Response:
            0,-0.001
        """
        self.send("GroupCurrentFollowingErrorGet("+GroupName+",double  *)")


    def GroupHomeSearch(self, GroupName: str):
        """Function argument(s) : GroupHomeSearch
        Start home search sequence
        char GroupName[251]
        GROUP1
        GROUP2
        GROUP3
        GROUP4
        GROUP5
        GROUP6
        GROUP1.POSITIONER
        GROUP2.POSITIONER
        GROUP3.POSITIONER
        GROUP4.POSITIONER
        GROUP5.POSITIONER
        GROUP6.POSITIONER

        Example Command Execution:
            GroupHomeSearch(GROUP1)
        Example Expected Response:
            -22,GroupHomeSearch(GROUP1)
        """
        self.send("GroupHomeSearch("+GroupName+")")


    def GroupHomeSearchAndRelativeMove(self, GroupName: str,
                                       TargetDisplacement: float):
        """Function argument(s) : GroupHomeSearchAndRelativeMove
        Start home search sequence and execute a displacement
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double TargetDisplacement

        0


        Example Command Execution:
            GroupHomeSearchAndRelativeMove(GROUP1,0)
        Example Expected Response:
            -22,GroupHomeSearchAndRelativeMove(GROUP1,0)
        """
        self.send("GroupHomeSearchAndRelativeMove("+GroupName+","+str(TargetDisplacement)+")")


    def GroupInitialize(self, GroupName: str):
        """Function argument(s) : GroupInitialize
        Start the initialization
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            GroupInitialize(GROUP1)
        Example Expected Response:
            0,
        """
        self.send("GroupInitialize("+GroupName+")")


    def GroupInitializeWithEncoderCalibration(self, GroupName: str):
        """Function argument(s) : GroupInitializeWithEncoderCalibration
        Start the initialization with encoder calibration
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            GroupInitializeWithEncoderCalibration(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupInitializeWithEncoderCalibration("+GroupName+")")


    def GroupJogCurrentGet(self, GroupName: str):
        """Function argument(s) : GroupJogCurrentGet
        Get Jog current on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* Velocity

        double* Velocity
        double* Acceleration

        double* Acceleration


        Example Command Execution:
            GroupJogCurrentGet(GROUP1,double *,double *)
        Example Expected Response:
            0,0,0
        """
        self.send("GroupJogCurrentGet("+GroupName+",double *,double *)")


    def GroupJogModeDisable(self, GroupName: str):
        """Function argument(s) : GroupJogModeDisable
        Disable Jog mode on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            GroupJogModeDisable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupJogModeDisable("+GroupName+")")


    def GroupJogModeEnable(self, GroupName: str):
        """Function argument(s) : GroupJogModeEnable
        Enable Jog mode on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            GroupJogModeEnable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupJogModeEnable("+GroupName+")")


    def GroupJogParametersGet(self, GroupName: str):
        """Function argument(s) : GroupJogParametersGet
        Get Jog parameters on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* Velocity

        double* Velocity
        double* Acceleration

        double* Acceleration


        Example Command Execution:
            GroupJogParametersGet(GROUP1,double *,double *)
        Example Expected Response:
            0,0,0
        """
        self.send("GroupJogParametersGet("+GroupName+",double *,double *)")


    def GroupJogParametersSet(self, GroupName: str, Velocity: float,
                              Acceleration: float):
        """Function argument(s) : GroupJogParametersSet
        Modify Jog parameters on selected group and activate the continuous move
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double Velocity

        0
        double Acceleration

        0


        Example Command Execution:
            GroupJogParametersSet(GROUP1,0,0)
        Example Expected Response:
            ?
        """
        self.send("GroupJogParametersSet("+GroupName+","+str(Velocity)+","+str(Acceleration)+")")


    def GroupKill(self, GroupName: str):
        """Function argument(s) : GroupKill
        Kill the group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            GroupKill(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupKill("+GroupName+")")


    def GroupMotionDisable(self, GroupName: str):
        """Function argument(s) : GroupMotionDisable
        Set Motion disable on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            GroupMotionDisable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupMotionDisable("+GroupName+")")


    def GroupMotionEnable(self, GroupName: str):
        """Function argument(s) : GroupMotionEnable
        Set Motion enable on selected group
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            GroupMotionEnable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupMotionEnable("+GroupName+")")


    def GroupMoveAbort(self, GroupName: str):
        """Function argument(s) : GroupMoveAbort
        Abort a move
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            GroupMoveAbort(GROUP1)
        Example Expected Response:
            -22,GroupMoveAbort(GROUP1)
        """
        self.send("GroupMoveAbort("+GroupName+")")


    def GroupMoveAbsolute(self, GroupName: str, TargetPosition: float):
        """Function argument(s) : GroupMoveAbsolute
        Do an absolute move
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double TargetPosition

        Example Command Execution:
            GroupMoveAbsolute(GROUP1,0)
        Example Expected Response:
            ?
        """
        self.send("GroupMoveAbsolute("+GroupName+","+str(TargetPosition)+")")


    def GroupMoveRelative(self, GroupName: str, TargetDisplacement: float):
        """Function argument(s) : GroupMoveRelative
        Do a relative move
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double TargetDisplacement

        0


        Example Command Execution:
            GroupMoveRelative(GROUP1,0)
        Example Expected Response:
            ?
        """
        self.send("GroupMoveRelative("+GroupName+","+str(TargetDisplacement)+")")


    def GroupPositionCorrectedProfilerGet(self, GroupName: str,
                                          PositionX: float, PositionY: float):
        """Function argument(s) : GroupPositionCorrectedProfilerGet
        Return corrected profiler positions
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double PositionX

        0
        double PositionY

        0
        double *CorrectedProfilerPositionX

        double *CorrectedProfilerPositionX
        double *CorrectedProfilerPositionY

        double *CorrectedProfilerPositionY


        Example Command Execution:
            GroupPositionCorrectedProfilerGet(GROUP1,0,0,double  *,double  *)
        Example Expected Response:
            ?
        """
        self.send("GroupPositionCorrectedProfilerGet("+GroupName+","+
                  str(PositionX)+","+str(PositionY)+",double  *,double  *)")


    def GroupPositionCurrentGet(self, GroupName: str):
        """Function argument(s) : GroupPositionCurrentGet
        Return current positions
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *CurrentEncoderPosition

        double *CurrentEncoderPosition

        Example Command Execution:
            GroupPositionCurrentGet(GROUP1,double  *)
        Example Expected Response:
            0,0
        """
        self.send("GroupPositionCurrentGet("+GroupName+",double  *)")


    def GroupPositionPCORawEncoderGet(self, GroupName: str, PositionX: float,
                                      PositionY: float):
        """Function argument(s) : GroupPositionPCORawEncoderGet
        Return PCO raw encoder positions
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double PositionX

        0
        double PositionY

        0
        double *PCORawPositionX

        double *PCORawPositionX
        double *PCORawPositionY

        double *PCORawPositionY


        Example Command Execution:
            GroupPositionPCORawEncoderGet(GROUP1,0,0,double  *,double  *)
        Example Expected Response:
            -18,GroupPositionPCORawEncoderGet(GROUP1,0,0,double  *,double  *)
        """
        self.send("GroupPositionPCORawEncoderGet("+GroupName+","+str(PositionX)+
                  ","+str(PositionY)+",double  *,double  *)")


    def GroupPositionSetpointGet(self, GroupName: str):
        """Function argument(s) : GroupPositionSetpointGet
        Return setpoint positions
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *SetPointPosition

        double *SetPointPosition


        Example Command Execution:
            GroupPositionSetpointGet(GROUP1,double  *)
        Example Expected Response:
            0,0
        """
        self.send("GroupPositionSetpointGet("+GroupName+",double  *)")


    def GroupPositionTargetGet(self, GroupName: str):
        """Function argument(s) : GroupPositionTargetGet
        Return target positions
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *TargetPosition

        double *TargetPosition

        Example Command Execution:
            GroupPositionTargetGet(GROUP1,double  *)
        Example Expected Response:
            0,0
        """
        self.send("GroupPositionTargetGet("+GroupName+",double  *)")


    def GroupReferencingActionExecute(self, PositionerName: str,
                                      ReferencingAction: str,
                                      ReferencingSensor: str,
                                      ReferencingParameter: float):
        """Function argument(s) : GroupReferencingActionExecute
        Execute an action in referencing mode
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char ReferencingAction[251]
            LatchOnLowToHighTransition
            LatchOnHighToLowTransition
            LatchOnIndex
            LatchOnIndexAfterSensorHighToLowTransition
            MoveToPreviouslyLatchedPosition
            MoveRelative
            SetPosition
            SetPositionToHomePreset
        char ReferencingSensor[251]
            MechanicalZero
            MinusEndOfRun
            PlusEndOfRun
            None
        double ReferencingParameter
        0


        Example Command Execution:
            GroupReferencingActionExecute(GROUP1,LatchOnLowToHighTransition,MechanicalZero,0)
        Example Expected Response:
            ?
        """
        self.send("GroupReferencingActionExecute("+PositionerName+","+
                  ReferencingAction+","+ReferencingSensor+","+
                  str(ReferencingParameter)+")")


    def GroupReferencingStart(self, GroupName: str):
        """Function argument(s) : GroupReferencingStart
        Enter referencing mode
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            GroupReferencingStart(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupReferencingStart("+GroupName+")")


    def GroupReferencingStop(self, GroupName: str):
        """Function argument(s) : GroupReferencingStop
        Exit referencing mode
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER


        Example Command Execution:
            GroupReferencingStop(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("GroupReferencingStop("+GroupName+")")


    def GroupStatusGet(self, GroupName: str):
        """Function argument(s) : GroupStatusGet
        Return group status
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        int *Status

        Example Command Execution:
            GroupStatusGet(GROUP1,int  *)
        Example Expected Response:
            0,42
        """
        self.send("GroupStatusGet("+GroupName+",int  *)")


    def GroupStatusStringGet(self, GroupStatusCode: int):
        """Function argument(s) : GroupStatusStringGet
        Return the group status string corresponding to the group status code
        int GroupStatusCode

        42
        char GroupStatusString[]

        char GroupStatusString[]


        Example Command Execution:
            GroupStatusStringGet(42,char *)
        Example Expected Response:
            0,Not referenced state
        """
        self.send("GroupStatusStringGet("+str(GroupStatusCode)+",char *)")


    def GroupVelocityCurrentGet(self, GroupName: str):
        """Function argument(s) : GroupVelocityCurrentGet
        Return current velocities
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *CurrentVelocity

        double *CurrentVelocity


        Example Command Execution:
            GroupVelocityCurrentGet(GROUP1,double  *)
        Example Expected Response:
            0,0
        """
        self.send("GroupVelocityCurrentGet("+GroupName+",double  *)")


    def HardwareDateAndTimeGet(self):
        """Function argument(s) : HardwareDateAndTimeGet
        Return hardware date and time
        char DateAndTime[]

        char DateAndTime[]


        Example Command Execution:
            HardwareDateAndTimeGet(char *)
        Example Expected Response:
            0,Thu Jun 28 11:21:13 2018
        """
        self.send("HardwareDateAndTimeGet(char *)")


    def HardwareDateAndTimeSet(self, DateAndTime: str):
        """Function argument(s) : HardwareDateAndTimeSet
        Set hardware date and time
        char DateAndTime[251]

        0


        Example Command Execution:
            HardwareDateAndTimeSet(0)
        Example Expected Response:
            ?
        """
        self.send("HardwareDateAndTimeSet("+DateAndTime+")")


    def KillAll(self):
        """Function argument(s) : KillAll
        Put all groups in 'Not initialized' state

        Example Command Execution:
            KillAll()
        Example Expected Response:
            0,
        """
        self.send("KillAll()")


    def Login(self, Name: str, Password: str):
        """Function argument(s) : Login
        Log in
        char Name[251]

        0
        char Password[251]

        0


        Example Command Execution:
            Login(0,0)
        Example Expected Response:
            -106,Login(0,0)
        """
        self.send("Login("+Name+","+Password+")")


    def PositionerAccelerationAutoScaling(self, PositionerName: str):
        """Function argument(s) : PositionerAccelerationAutoScaling
        Astrom&Hagglund based auto-scaling
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* Scaling

        double* Scaling

        Example Command Execution:
            PositionerAccelerationAutoScaling(GROUP1,double *)
        Example Expected Response:
            ?
        """
        self.send("PositionerAccelerationAutoScaling("+PositionerName+",double *)")


    def PositionerAnalogTrackingPositionParametersGet(self, PositionerName: str):
        """Function argument(s) : PositionerAnalogTrackingPositionParametersGet
        Read dynamic parameters for one axe of a group for a future analog tracking position
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char GPIOName[]

        char GPIOName[]
        double* Offset

        double* Offset
        double* Scale

        double* Scale
        double* Velocity

        double* Velocity
        double* Acceleration

        double* Acceleration


        Example Command Execution:
            PositionerAnalogTrackingPositionParametersGet(GROUP1,char *,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerAnalogTrackingPositionParametersGet(GROUP1,char *,double *,double *,double *,double *)
        """
        self.send("PositionerAnalogTrackingPositionParametersGet("+
                  PositionerName+",char *,double *,double *,double *,double *)")


    def PositionerAnalogTrackingPositionParametersSet(self, PositionerName: str,
                                                      GPIOName: str,
                                                      Offset: float,
                                                      Scale: float,
                                                      Velocity: float,
                                                      Acceleration: float):
        """Function argument(s) : PositionerAnalogTrackingPositionParametersSet
        Update dynamic parameters for one axe of a group for a future analog tracking position
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        double Offset

        0
        double Scale

        0
        double Velocity

        0
        double Acceleration

        0


        Example Command Execution:
            PositionerAnalogTrackingPositionParametersSet(GROUP1,GPIO2.ADC1,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerAnalogTrackingPositionParametersSet("+
                  PositionerName+","+GPIOName+","+str(Offset)+","+
                  str(Scale)+","+str(Velocity)+","+str(Acceleration)+")")


    def PositionerAnalogTrackingVelocityParametersGet(self, PositionerName: str):
        """Function argument(s) : PositionerAnalogTrackingVelocityParametersGet
        Read dynamic parameters for one axe of a group for a future analog tracking velocity
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char GPIOName[]

        char GPIOName[]
        double* Offset

        double* Offset
        double* Scale

        double* Scale
        double* DeadBandThreshold

        double* DeadBandThreshold
        int* Order

        int* Order
        double* Velocity

        double* Velocity
        double* Acceleration

        double* Acceleration

        Example Command Execution:
            PositionerAnalogTrackingVelocityParametersGet(GROUP1,char *,double *,double *,double *,int *,double *,double *)
        Example Expected Response:
            -18,PositionerAnalogTrackingVelocityParametersGet(GROUP1,char *,double *,double *,double *,int *,double *,double *)
        """
        self.send("PositionerAnalogTrackingVelocityParametersGet("+
                  PositionerName+
                  ",char *,double *,double *,double *,int *,double *,double *)")


    def PositionerAnalogTrackingVelocityParametersSet(self, PositionerName: str,
                                                      GPIOName: str,
                                                      Offset: float,
                                                      Scale: float,
                                                      DeadBandThreshold: float,
                                                      Order: int,
                                                      Velocity: float,
                                                      Acceleration: float):
        """Function argument(s) : PositionerAnalogTrackingVelocityParametersSet
        Update dynamic parameters for one axe of a group for a future analog tracking velocity
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char GPIOName[251]
            GPIO2.ADC1
            GPIO2.ADC2
            GPIO2.ADC3
            GPIO2.ADC4
            GPIO2.DAC1
            GPIO2.DAC2
            GPIO2.DAC3
            GPIO2.DAC4
            GPIO1.DI
            GPIO2.DI
            GPIO3.DI
            GPIO4.DI
            GPIO1.DO
            GPIO3.DO
            GPIO4.DO
        double Offset

        0
        double Scale

        0
        double DeadBandThreshold

        0
        int Order

        0
        double Velocity

        0
        double Acceleration

        0


        Example Command Execution:
            PositionerAnalogTrackingVelocityParametersSet(GROUP1,GPIO2.ADC1,0,0,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerAnalogTrackingVelocityParametersSet("+
                  PositionerName+","+GPIOName+","+str(Offset)+","+
                  str(Scale)+","+str(DeadBandThreshold)+","+str(Order)+","+
                  str(Velocity)+","+str(Acceleration)+")")


    def PositionerBacklashDisable(self, PositionerName: str):
        """Function argument(s) : PositionerBacklashDisable
        Disable the backlash
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            PositionerBacklashDisable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("PositionerBacklashDisable("+PositionerName+")")


    def PositionerBacklashEnable(self, PositionerName: str):
        """Function argument(s) : PositionerBacklashEnable
        Enable the backlash
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            PositionerBacklashEnable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("PositionerBacklashEnable("+PositionerName+")")


    def PositionerBacklashGet(self, PositionerName: str):
        """Function argument(s) : PositionerBacklashGet
        Read backlash value and status
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* BacklashValue

        double* BacklashValue
        char BacklaskStatus[]

        char BacklaskStatus[]

        Example Command Execution:
            PositionerBacklashGet(GROUP1,double *,char *)
        Example Expected Response:
            -18,PositionerBacklashGet(GROUP1,double *,char *)
        """
        self.send("PositionerBacklashGet("+PositionerName+",double *,char *)")


    def PositionerBacklashSet(self, PositionerName: str, BacklashValue: float):
        """Function argument(s) : PositionerBacklashSet
        Set backlash value
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double BacklashValue
        0

        Example Command Execution:
            PositionerBacklashSet(GROUP1,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerBacklashSet("+PositionerName+","+
                  str(BacklashValue)+")")


    def PositionerCorrectorAutoTuning(self, PositionerName: str, TuningMode: int):
        """Function argument(s) : PositionerCorrectorAutoTuning
        Astrom&Hagglund based auto-tuning
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        int TuningMode

        0
        double* KP

        double* KP
        double* KI

        double* KI
        double* KD

        double* KD

        Example Command Execution:
            PositionerCorrectorAutoTuning(GROUP1,0,double *,double *,double *)
        Example Expected Response:
            ?
        """
        self.send("PositionerCorrectorAutoTuning("+PositionerName+","+
                  str(TuningMode)+",double *,double *,double *)")


    def PositionerCorrectorNotchFiltersGet(self, PositionerName: str):
        """Function argument(s) : PositionerCorrectorNotchFiltersGet
        Read filters parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* NotchFrequency1

        double* NotchFrequency1
        double* NotchBandwith1

        double* NotchBandwith1
        double* NotchGain1

        double* NotchGain1
        double* NotchFrequency2

        double* NotchFrequency2
        double* NotchBandwith2

        double* NotchBandwith2
        double* NotchGain2

        double* NotchGain2


        Example Command Execution:
            PositionerCorrectorNotchFiltersGet(GROUP1,double *,double *,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerCorrectorNotchFiltersGet(GROUP1,double *,double *,double *,double *,double *,double *)
        """
        self.send("PositionerCorrectorNotchFiltersGet("+PositionerName+
                  ",double *,double *,double *,double *,double *,double *)")


    def PositionerCorrectorNotchFiltersSet(self, PositionerName: str,
                                           NotchFrequency1: float,
                                           NotchBandwith1: float,
                                           NotchGain1: float,
                                           NotchFrequency2: float,
                                           NotchBandwith2: float,
                                           NotchGain2: float):
        """Function argument(s) : PositionerCorrectorNotchFiltersSet
        Update filters parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double NotchFrequency1

        0
        double NotchBandwith1

        0
        double NotchGain1

        0
        double NotchFrequency2

        0
        double NotchBandwith2

        0
        double NotchGain2

        0


        Example Command Execution:
            PositionerCorrectorNotchFiltersSet(GROUP1,0,0,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerCorrectorNotchFiltersSet("+PositionerName+","+
                  str(NotchFrequency1)+","+str(NotchBandwith1)+","+
                  str(NotchGain1)+","+str(NotchFrequency2)+","+
                  str(NotchBandwith2)+","+str(NotchGain2)+")")


    def PositionerCorrectorPIDDualFFVoltageGet(self, PositionerName: str):
        """Function argument(s) : PositionerCorrectorPIDDualFFVoltageGet
        Read corrector parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        bool* ClosedLoopStatus

        bool* ClosedLoopStatus
        double* KP

        double* KP
        double* KI

        double* KI
        double* KD

        double* KD
        double* KS

        double* KS
        double* IntegrationTime

        double* IntegrationTime
        double* DerivativeFilterCutOffFrequency

        double* DerivativeFilterCutOffFrequency
        double* GKP

        double* GKP
        double* GKI

        double* GKI
        double* GKD

        double* GKD
        double* KForm

        double* KForm
        double* FeedForwardGainVelocity

        double* FeedForwardGainVelocity
        double* FeedForwardGainAcceleration

        double* FeedForwardGainAcceleration
        double* Friction

        double* Friction


        Example Command Execution:
            PositionerCorrectorPIDDualFFVoltageGet(GROUP1,bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerCorrectorPIDDualFFVoltageGet(GROUP1,bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)
        """
        self.send("PositionerCorrectorPIDDualFFVoltageGet("+PositionerName+
                  ",bool *,double *,double *,double *,double *,double *,"+
                  "double *,double *,double *,double *,double *,double *,"+
                  "double *,double *)")


    def PositionerCorrectorPIDDualFFVoltageSet(self, PositionerName: str,
                                               ClosedLoopStatus: bool,
                                               KP: float,
                                               KI: float,
                                               KD: float,
                                               KS: float,
                                               IntegrationTime: float,
                                               DerivativeFilterCutOffFreq: float,
                                               GKP: float,
                                               GKI: float,
                                               GKD: float,
                                               KForm: float,
                                               FeedForwardGainVelocity: float,
                                               FeedForwardGainAcc: float,
                                               Friction: float):
        """Function argument(s) : PositionerCorrectorPIDDualFFVoltageSet
        Update corrector parameters
        char PositionerName[251]

        bool ClosedLoopStatus

        0
        double KP

        0
        double KI

        0
        double KD

        0
        double KS

        0
        double IntegrationTime

        0
        double DerivativeFilterCutOffFrequency

        0
        double GKP

        0
        double GKI

        0
        double GKD

        0
        double KForm

        0
        double FeedForwardGainVelocity

        0
        double FeedForwardGainAcceleration

        0
        double Friction

        0

            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            PositionerCorrectorPIDDualFFVoltageSet(GROUP1,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerCorrectorPIDDualFFVoltageSet("+PositionerName+","+
                  str(ClosedLoopStatus)+","+str(KP)+","+str(KI)+","+str(KD)+","+
                  str(KS)+","+str(IntegrationTime)+","+
                  str(DerivativeFilterCutOffFreq)+","+str(GKP)+","+str(GKI)+","+
                  str(GKD)+","+str(KForm)+","+str(FeedForwardGainVelocity)+","+
                  str(FeedForwardGainAcc)+","+str(Friction)+")")


    def PositionerCorrectorPIDFFAccelerationGet(self, PositionerName: str):
        """Function argument(s) : PositionerCorrectorPIDFFAccelerationGet
        Read corrector parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        bool* ClosedLoopStatus

        bool* ClosedLoopStatus
        double* KP

        double* KP
        double* KI

        double* KI
        double* KD

        double* KD
        double* KS

        double* KS
        double* IntegrationTime

        double* IntegrationTime
        double* DerivativeFilterCutOffFrequency

        double* DerivativeFilterCutOffFrequency
        double* GKP

        double* GKP
        double* GKI

        double* GKI
        double* GKD

        double* GKD
        double* KForm

        double* KForm
        double* FeedForwardGainAcceleration

        double* FeedForwardGainAcceleration

        Example Command Execution:
            PositionerCorrectorPIDFFAccelerationGet(GROUP1,bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerCorrectorPIDFFAccelerationGet(GROUP1,bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)
        """
        self.send("PositionerCorrectorPIDFFAccelerationGet("+PositionerName+
                  ",bool *,double *,double *,double *,double *,double *,"+
                  "double *,double *,double *,double *,double *,double *)")


    def PositionerCorrectorPIDFFAccelerationSet(self, PositionerName: str,
                                                ClosedLoopStatus: bool,
                                                KP: float,
                                                KI: float,
                                                KD: float,
                                                KS: float,
                                                IntegrationTime: float,
                                                DerivativeFilterCutOffFreq: float,
                                                GKP: float,
                                                GKI: float,
                                                GKD: float,
                                                KForm: float,
                                                FeedForwardGainAcc: float):
        """Function argument(s) : PositionerCorrectorPIDFFAccelerationSet
        Update corrector parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        bool ClosedLoopStatus

        0
        double KP

        0
        double KI

        0
        double KD

        0
        double KS

        0
        double IntegrationTime

        0
        double DerivativeFilterCutOffFrequency

        0
        double GKP

        0
        double GKI

        0
        double GKD

        0
        double KForm

        0
        double FeedForwardGainAcceleration

        0


        Example Command Execution:
            PositionerCorrectorPIDFFAccelerationSet(GROUP1,0,0,0,0,0,0,0,0,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerCorrectorPIDFFAccelerationSet("+PositionerName+","+
                  str(ClosedLoopStatus)+","+str(KP)+","+str(KI)+","+str(KD)+","+
                  str(KS)+","+str(IntegrationTime)+","+
                  str(DerivativeFilterCutOffFreq)+","+str(GKP)+","+str(GKI)+","+
                  str(GKD)+","+str(KForm)+","+str(FeedForwardGainAcc)+")")


    def PositionerCorrectorPIDFFVelocityGet(self, PositionerName: str):
        """Function argument(s) : PositionerCorrectorPIDFFVelocityGet
        Read corrector parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        bool* ClosedLoopStatus

        bool* ClosedLoopStatus
        double* KP

        double* KP
        double* KI

        double* KI
        double* KD

        double* KD
        double* KS

        double* KS
        double* IntegrationTime

        double* IntegrationTime
        double* DerivativeFilterCutOffFrequency

        double* DerivativeFilterCutOffFrequency
        double* GKP

        double* GKP
        double* GKI

        double* GKI
        double* GKD

        double* GKD
        double* KForm

        double* KForm
        double* FeedForwardGainVelocity

        double* FeedForwardGainVelocity


        Example Command Execution:
            PositionerCorrectorPIDFFVelocityGet(GROUP1,bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerCorrectorPIDFFVelocityGet(GROUP1,bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)
        """
        self.send("PositionerCorrectorPIDFFVelocityGet("+PositionerName+
                  ",bool *,double *,double *,double *,double *,double *,"+
                  "double *,double *,double *,double *,double *,double *)")


    def PositionerCorrectorPIDFFVelocitySet(self, PositionerName: str,
                                            ClosedLoopStatus: bool,
                                            KP: float,
                                            KI: float,
                                            KD: float,
                                            KS: float,
                                            IntegrationTime: float,
                                            DerivativeFilterCutOffFreq: float,
                                            GKP: float,
                                            GKI: float,
                                            GKD: float,
                                            KForm: float,
                                            FeedForwardGainVelocity: float):
        """Function argument(s) : PositionerCorrectorPIDFFVelocitySet
        Update corrector parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        bool ClosedLoopStatus

        0
        double KP

        0
        double KI

        0
        double KD

        0
        double KS

        0
        double IntegrationTime

        0
        double DerivativeFilterCutOffFrequency

        0
        double GKP

        0
        double GKI

        0
        double GKD

        0
        double KForm

        0
        double FeedForwardGainVelocity

        0


        Example Command Execution:
            PositionerCorrectorPIDFFVelocitySet(GROUP1,0,0,0,0,0,0,0,0,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerCorrectorPIDFFVelocitySet("+PositionerName+","+
                  str(ClosedLoopStatus)+","+str(KP)+","+str(KI)+","+
                  str(KD)+","+str(KS)+","+str(IntegrationTime)+","+
                  str(DerivativeFilterCutOffFreq)+","+str(GKP)+","+
                  str(GKI)+","+str(GKD)+","+str(KForm)+","+
                  str(FeedForwardGainVelocity)+")")


    def PositionerCorrectorPIPositionGet(self, PositionerName: str):
        """Function argument(s) : PositionerCorrectorPIPositionGet
        Read corrector parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        bool* ClosedLoopStatus

        bool* ClosedLoopStatus
        double* KP

        double* KP
        double* KI

        double* KI
        double* IntegrationTime

        double* IntegrationTime


        Example Command Execution:
            PositionerCorrectorPIPositionGet(GROUP1,bool *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerCorrectorPIPositionGet(GROUP1,bool *,double *,double *,double *)

        """
        self.send("PositionerCorrectorPIPositionGet("+PositionerName+
                  ",bool *,double *,double *,double *)")


    def PositionerCorrectorPIPositionSet(self, PositionerName: str,
                                         ClosedLoopStatus: bool, KP: float,
                                         KI: float, IntegrationTime: float):
        """Function argument(s) : PositionerCorrectorPIPositionSet
        Update corrector parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        bool ClosedLoopStatus

        0
        double KP

        0
        double KI

        0
        double IntegrationTime

        0


        Example Command Execution:
            PositionerCorrectorPIPositionSet(GROUP1,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerCorrectorPIPositionSet("+PositionerName+","+
                  ClosedLoopStatus+","+KP+","+KI+","+IntegrationTime+")")


    def PositionerCorrectorTypeGet(self, PositionerName: str):
        """Function argument(s) : PositionerCorrectorTypeGet
        Read corrector type
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        char CorrectorType[]

        char CorrectorType[]

        Example Command Execution:
            PositionerCorrectorTypeGet(GROUP1,char *)
        Example Expected Response:
            -18,PositionerCorrectorTypeGet(GROUP1,char *)
        """
        self.send("PositionerCorrectorTypeGet("+PositionerName+",char *)")


    def PositionerCurrentVelocityAccelerationFiltersGet(self, PositionerName: str):
        """Function argument(s) : PositionerCurrentVelocityAccelerationFiltersGet
        Get current velocity and acceleration cutoff frequencies
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *CurrentVelocityCutOffFrequency

        double *CurrentVelocityCutOffFrequency
        double *CurrentAccelerationCutOffFrequency

        double *CurrentAccelerationCutOffFrequency

        Example Command Execution:
            PositionerCurrentVelocityAccelerationFiltersGet(GROUP1,double  *,double  *)
        Example Expected Response:
            -18,PositionerCurrentVelocityAccelerationFiltersGet(GROUP1,double  *,double  *)
        """
        self.send("PositionerCurrentVelocityAccelerationFiltersGet("+
                  PositionerName+",double  *,double  *)")


    def PositionerCurrentVelocityAccelerationFiltersSet(
            self,
            PositionerName: str,
            CurrentVelocityCutOffFrequency: float,
            CurrentAccelerationCutOffFrequency: float):
        """Function argument(s) : PositionerCurrentVelocityAccelerationFiltersSet
        Set current velocity and acceleration cutoff frequencies
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double CurrentVelocityCutOffFrequency

        0
        double CurrentAccelerationCutOffFrequency

        0
        Example Command Execution:
            PositionerCurrentVelocityAccelerationFiltersSet(GROUP1,0,0)
        Example Expected Response:
            ?
        """
        self.send(
            "PositionerCurrentVelocityAccelerationFiltersSet("+
            PositionerName+","+str(CurrentVelocityCutOffFrequency)+","+
            str(CurrentAccelerationCutOffFrequency)+")")


    def PositionerDriverFiltersGet(self, PositionerName: str):
        """Function argument(s) : PositionerDriverFiltersGet
        Get driver filters parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *KI

        double *KI
        double *NotchFrequency

        double *NotchFrequency
        double *NotchBandwidth

        double *NotchBandwidth
        double *NotchGain

        double *NotchGain
        double *LowpassFrequency

        double *LowpassFrequency


        Example Command Execution:
            PositionerDriverFiltersGet(GROUP1,double  *,double  *,double  *,double  *,double  *)
        Example Expected Response:
            -18,PositionerDriverFiltersGet(GROUP1,double  *,double  *,double  *,double  *,double  *)
        """
        self.send("PositionerDriverFiltersGet("+PositionerName+
                  ",double  *,double  *,double  *,double  *,double  *)")


    def PositionerDriverFiltersSet(self, PositionerName: str, KI: float,
                                   NotchFrequency: float, NotchBandwidth: float,
                                   NotchGain: float, LowpassFrequency: float):
        """Function argument(s) : PositionerDriverFiltersSet
        Set driver filters parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double KI

        0
        double NotchFrequency

        0
        double NotchBandwidth

        0
        double NotchGain

        0
        double LowpassFrequency

        0


        Example Command Execution:
            PositionerDriverFiltersSet(GROUP1,0,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerDriverFiltersSet("+PositionerName+","+str(KI)+","+
                  str(NotchFrequency)+","+str(NotchBandwidth)+","+
                  str(NotchGain)+","+str(LowpassFrequency)+")")


    def PositionerDriverPositionOffsetsGet(self, PositionerName: str):
        """Function argument(s) : PositionerDriverPositionOffsetsGet
        Get driver stage and gage position offset
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double *StagePositionOffset

        double *StagePositionOffset
        double *GagePositionOffset

        double *GagePositionOffset


        Example Command Execution:
            PositionerDriverPositionOffsetsGet(GROUP1,double  *,double  *)
        Example Expected Response:
            -18,PositionerDriverPositionOffsetsGet(GROUP1,double  *,double  *)
        """
        self.send("PositionerDriverPositionOffsetsGet("+PositionerName+",double  *,double  *)")


    def PositionerDriverStatusGet(self, PositionerName: str):
        """Function argument(s) : PositionerDriverStatusGet
        Read positioner driver status
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        int *DriverStatus


        Example Command Execution:
            PositionerDriverStatusGet(GROUP1,int  *)
        Example Expected Response:
            -18,PositionerDriverStatusGet(GROUP1,int  *)
        """
        self.send("PositionerDriverStatusGet("+PositionerName+",int  *)")


    def PositionerDriverStatusStringGet(self, PositionerDriverStatus: int):
        """Function argument(s) : PositionerDriverStatusStringGet
        Return the positioner driver status string corresponding to the positioner error code
        int PositionerDriverStatus

        0
        char PositionerDriverStatusString[]

        char PositionerDriverStatusString[]

        Example Command Execution:
            PositionerDriverStatusStringGet(0,char *)
        Example Expected Response:
            ?
        """
        self.send("PositionerDriverStatusStringGet("+str(PositionerDriverStatus)+",char *)")


    def PositionerEncoderAmplitudeValuesGet(self, PositionerName: str):
        """Function argument(s) : PositionerEncoderAmplitudeValuesGet
        Read analog interpolated encoder amplitude values
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* CalibrationSinusAmplitude

        double* CalibrationSinusAmplitude
        double* CurrentSinusAmplitude

        double* CurrentSinusAmplitude
        double* CalibrationCosinusAmplitude

        double* CalibrationCosinusAmplitude
        double* CurrentCosinusAmplitude

        double* CurrentCosinusAmplitude

        Example Command Execution:
            PositionerEncoderAmplitudeValuesGet(GROUP1,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerEncoderAmplitudeValuesGet(GROUP1,double *,double *,double *,double *)
        """
        self.send("PositionerEncoderAmplitudeValuesGet("+PositionerName+
                  ",double *,double *,double *,double *)")


    def PositionerEncoderCalibrationParametersGet(self, PositionerName: str):
        """Function argument(s) : PositionerEncoderCalibrationParametersGet
        Read analog interpolated encoder calibration parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* SinusOffset

        double* SinusOffset
        double* CosinusOffset

        double* CosinusOffset
        double* DifferentialGain

        double* DifferentialGain
        double* PhaseCompensation

        double* PhaseCompensation

        Example Command Execution:
            PositionerEncoderCalibrationParametersGet(GROUP1,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerEncoderCalibrationParametersGet(GROUP1,double *,double *,double *,double *)
        """
        self.send("PositionerEncoderCalibrationParametersGet("+PositionerName+
                  ",double *,double *,double *,double *)")


    def PositionerErrorGet(self, PositionerName: str):
        """Function argument(s) : PositionerErrorGet
        Read and clear positioner error code
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        int *ErrorCode

        int *ErrorCode

        Example Command Execution:
            PositionerErrorGet(GROUP1,int  *)
        Example Expected Response:
            -18,PositionerErrorGet(GROUP1,int  *)
        """
        self.send("PositionerErrorGet("+PositionerName+",int  *)")


    def PositionerErrorRead(self, PositionerName: str):
        """Function argument(s) : PositionerErrorRead
        Read only positioner error code without clear it
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        int *ErrorCode

        int *ErrorCode

        Example Command Execution:
            PositionerErrorRead(GROUP1,int  *)
        Example Expected Response:
            -18,PositionerErrorRead(GROUP1,int  *)
        """
        self.send("PositionerErrorRead("+PositionerName+",int  *)")


    def PositionerErrorStringGet(self, PositionerErrorCode: int):
        """Function argument(s) :
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            PositionerErrorStringGet(0,char *)
        Example Expected Response:
            0,
        """
        self.send("PositionerErrorStringGet("+str(PositionerErrorCode)+",char *)")


    def PositionerExcitationSignalGet(self, PositionerName: str):
        """Function argument(s) : PositionerExcitationSignalGet
        Read disturbing signal parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        int* Mode

        int* Mode
        double* Frequency

        double* Frequency
        double* Amplitude

        double* Amplitude
        double* Time

        double* Time


        Example Command Execution:
            PositionerExcitationSignalGet(GROUP1,int *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerExcitationSignalGet(GROUP1,int *,double *,double *,double *)
        """
        self.send("PositionerExcitationSignalGet("+PositionerName+
                  ",int *,double *,double *,double *)")


    def PositionerExcitationSignalSet(self, PositionerName: str, Mode: int,
                                      Frequency: float, Amplitude: float,
                                      Time: float):
        """Function argument(s) : PositionerExcitationSignalSet
        Update disturbing signal parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        int Mode

        0
        double Frequency

        0
        double Amplitude

        0
        double Time

        0

        Example Command Execution:
            PositionerExcitationSignalSet(GROUP1,0,0,0,0)
        Example Expected Response:
            0
        """
        self.send("PositionerExcitationSignalSet("+PositionerName+","+
                  str(Mode)+","+str(Frequency)+","+str(Amplitude)+","+
                  str(Time)+")")


    def PositionerExternalLatchPositionGet(self, PositionerName: str):
        """Function argument(s) : PositionerExternalLatchPositionGet
        Read external latch position
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* Position

        double* Position

        Example Command Execution:
            PositionerExternalLatchPositionGet(GROUP1,double *)
        Example Expected Response:
            -18,PositionerExternalLatchPositionGet(GROUP1,double *)
        """
        self.send("PositionerExternalLatchPositionGet("+PositionerName+",double *)")


    def PositionerHardInterpolatorFactorGet(self, PositionerName: str):
        """Function argument(s) : PositionerHardInterpolatorFactorGet
        Get hard interpolator parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        int * InterpolationFactor

        int * InterpolationFactor

        Example Command Execution:
            PositionerHardInterpolatorFactorGet(GROUP1,int  *)
        Example Expected Response:
            -18,PositionerHardInterpolatorFactorGet(GROUP1,int  *)
        """
        self.send("PositionerHardInterpolatorFactorGet("+PositionerName+",int  *)")


    def PositionerHardInterpolatorFactorSet(self, PositionerName: str,
                                            InterpolationFactor: int):
        """Function argument(s) : PositionerHardInterpolatorFactorSet
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Set hard interpolator parameters
        char PositionerName[251]

        int InterpolationFactor

        0

        Example Command Execution:
            PositionerHardInterpolatorFactorSet(GROUP1,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerHardInterpolatorFactorSet("+PositionerName+","+
                  str(InterpolationFactor)+")")


    def PositionerHardwareStatusGet(self, PositionerName: str):
        """Function argument(s) : PositionerHardwareStatusGet
        Read positioner hardware status
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        int *HardwareStatus

        int *HardwareStatus

        Example Command Execution:
            PositionerHardwareStatusGet(GROUP1,int  *)
        Example Expected Response:
            -18,PositionerHardwareStatusGet(GROUP1,int  *)
        """
        self.send("PositionerHardwareStatusGet("+PositionerName+",int  *)")


    def PositionerHardwareStatusStringGet(self, PositionerHardwareStatus: int):
        """Function argument(s) : PositionerHardwareStatusStringGet
        Return the positioner hardware status string corresponding to the positioner error code
        int PositionerHardwareStatus

        0
        char PositionerHardwareStatusString[]

        char PositionerHardwareStatusString[]

        Example Command Execution:
            PositionerHardwareStatusStringGet(0,char *)
        Example Expected Response:
            0,ZM low level
        """
        self.send("PositionerHardwareStatusStringGet("+str(PositionerHardwareStatus)+",char *)")


    def PositionerMaximumVelocityAndAccelerationGet(self, PositionerName: str):
        """Function argument(s) : PositionerMaximumVelocityAndAccelerationGet
        Return maximum velocity and acceleration of the positioner
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* MaximumVelocity

        double* MaximumVelocity
        double* MaximumAcceleration

        double* MaximumAcceleration


        Example Command Execution:
            PositionerMaximumVelocityAndAccelerationGet(GROUP1,double *,double *)
        Example Expected Response:
            -18,PositionerMaximumVelocityAndAccelerationGet(GROUP1,double *,double *)
        """
        self.send("PositionerMaximumVelocityAndAccelerationGet("+PositionerName+
                  ",double *,double *)")


    def PositionerMotionDoneGet(self, PositionerName: str):
        """Function argument(s) : PositionerMotionDoneGet
        Read motion done parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* PositionWindow

        double* PositionWindow
        double* VelocityWindow

        double* VelocityWindow
        double* CheckingTime

        double* CheckingTime
        double* MeanPeriod

        double* MeanPeriod
        double* TimeOut

        double* TimeOut


        Example Command Execution:
            PositionerMotionDoneGet(GROUP1,double *,double *,double *,double *,double *)
        Example Expected Response:
            -18,PositionerMotionDoneGet(GROUP1,double *,double *,double *,double *,double *)\
        """
        self.send("PositionerMotionDoneGet("+PositionerName+
                  ",double *,double *,double *,double *,double *)")


    def PositionerMotionDoneSet(self, PositionerName: str,
                                PositionWindow: float, VelocityWindow: float,
                                CheckingTime: float, MeanPeriod: float,
                                TimeOut: float):
        """Function argument(s) : PositionerMotionDoneSet
        Update motion done parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double PositionWindow

        0
        double VelocityWindow

        0
        double CheckingTime

        0
        double MeanPeriod

        0
        double TimeOut

        0


        Example Command Execution:
            PositionerMotionDoneSet(GROUP1,0,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerMotionDoneSet("+PositionerName+","+
                  PositionWindow+","+VelocityWindow+","+CheckingTime+","+
                  MeanPeriod+","+TimeOut+")")


    def PositionerPositionCompareAquadBAlwaysEnable(self, PositionerName: str):
        """Function argument(s) : PositionerPositionCompareAquadBAlwaysEnable
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Enable AquadB signal in always mode
        char PositionerName[251]


        Example Command Execution:
            PositionerPositionCompareAquadBAlwaysEnable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("PositionerPositionCompareAquadBAlwaysEnable("+PositionerName+")")


    def PositionerPositionCompareAquadBWindowedGet(self, PositionerName: str):
        """Function argument(s) : PositionerPositionCompareAquadBWindowedGet
        Read position compare AquadB windowed parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* MinimumPosition

        double* MinimumPosition
        double* MaximumPosition

        double* MaximumPosition
        bool * EnableState

        bool * EnableState


        Example Command Execution:
            PositionerPositionCompareAquadBWindowedGet(GROUP1,double *,double *,bool  *)
        Example Expected Response:
            -18,PositionerPositionCompareAquadBWindowedGet(GROUP1,double *,double *,bool  *)
        """
        self.send("PositionerPositionCompareAquadBWindowedGet("+PositionerName+
                  ",double *,double *,bool  *)")


    def PositionerPositionCompareAquadBWindowedSet(self, PositionerName: str,
                                                   MinimumPosition: float,
                                                   MaximumPosition: float):
        """Function argument(s) : PositionerPositionCompareAquadBWindowedSet
        Set position compare AquadB windowed parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER


        double MinimumPosition

        0
        double MaximumPosition

        0

        Example Command Execution:
            PositionerPositionCompareAquadBWindowedSet(GROUP1,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerPositionCompareAquadBWindowedSet("+
                  PositionerName+","+str(MinimumPosition)+","+
                  str(MaximumPosition)+")")


    def PositionerPositionCompareDisable(self, PositionerName: str):
        """Function argument(s) : PositionerPositionCompareDisable
        Disable position compare
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            PositionerPositionCompareDisable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("PositionerPositionCompareDisable("+PositionerName+")")


    def PositionerPositionCompareEnable(self, PositionerName: str):
        """Function argument(s) : PositionerPositionCompareEnable
        Enable position compare
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            PositionerPositionCompareEnable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("PositionerPositionCompareEnable("+PositionerName+")")


    def PositionerPositionCompareGet(self, PositionerName: str):
        """Function argument(s) : PositionerPositionCompareGet
        Read position compare parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* MinimumPosition

        double* MinimumPosition
        double* MaximumPosition

        double* MaximumPosition
        double* PositionStep

        double* PositionStep
        bool * EnableState

        bool * EnableState


        Example Command Execution:
            PositionerPositionCompareGet(GROUP1,double *,double *,double *,bool  *)
        Example Expected Response:
            -18,PositionerPositionCompareGet(GROUP1,double *,double *,double *,bool  *)
        """
        self.send("PositionerPositionCompareGet("+PositionerName+
                  ",double *,double *,double *,bool  *)")


    def PositionerPositionComparePulseParametersGet(self, PositionerName: str):
        """Function argument(s) : PositionerPositionComparePulseParametersGet
        Get position compare PCO pulse parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* PCOPulseWidth

        double* PCOPulseWidth
        double* EncoderSettlingTime

        double* EncoderSettlingTime


        Example Command Execution:
            PositionerPositionComparePulseParametersGet(GROUP1,double *,double *)
        Example Expected Response:
            -18,PositionerPositionComparePulseParametersGet(GROUP1,double *,double *)
        """
        self.send("PositionerPositionComparePulseParametersGet("+PositionerName+
                  ",double *,double *)")


    def PositionerPositionComparePulseParametersSet(self, PositionerName: str,
                                                    PCOPulseWidth: float,
                                                    EncoderSettlingTime: float):
        """Function argument(s) : PositionerPositionComparePulseParametersSet
        Set position compare PCO pulse parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double PCOPulseWidth

        0
        double EncoderSettlingTime

        0


        Example Command Execution:
            PositionerPositionComparePulseParametersSet(GROUP1,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerPositionComparePulseParametersSet("+
                  PositionerName+","+str(PCOPulseWidth)+","+
                  str(EncoderSettlingTime)+")")


    def PositionerPositionCompareSet(self, PositionerName: str,
                                     MinimumPosition: float,
                                     MaximumPosition: float,
                                     PositionStep: float):
        """Function argument(s) : PositionerPositionCompareSet
        Set position compare parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double MinimumPosition

        0
        double MaximumPosition

        0
        double PositionStep

        0


        Example Command Execution:
            PositionerPositionCompareSet(GROUP1,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerPositionCompareSet("+PositionerName+","+
                  str(MinimumPosition)+","+str(MaximumPosition)+","+
                  str(PositionStep)+")")


    def PositionerRawEncoderPositionGet(self, PositionerName: str,
                                        UserEncoderPosition: float):
        """Function argument(s) : PositionerRawEncoderPositionGet
        Get the raw encoder position
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double UserEncoderPosition

        0
        double* RawEncoderPosition

        double* RawEncoderPosition

        Example Command Execution:
            PositionerRawEncoderPositionGet(GROUP1,0,double *)
        Example Expected Response:
            -18,PositionerRawEncoderPositionGet(GROUP1,0,double *)
        """
        self.send("PositionerRawEncoderPositionGet("+PositionerName+","+
                  str(UserEncoderPosition)+",double *)")


    def PositionersEncoderIndexDifferenceGet(self, PositionerName: str):
        """Function argument(s) : PositionersEncoderIndexDifferenceGet
        Return the difference between index of primary axis and secondary axis (only after homesearch)
        char PositionerName[251]

        double *distance

        double *distance

            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            PositionersEncoderIndexDifferenceGet(GROUP1,double  *)
        Example Expected Response:
            -18,PositionersEncoderIndexDifferenceGet(GROUP1,double  *)
        """
        self.send("PositionersEncoderIndexDifferenceGet("+PositionerName+",double  *)")


    def PositionerSGammaExactVelocityAjustedDisplacementGet(self,
                                                            PositionerName: str,
                                                            DesiredDisplacement: float):
        """Function argument(s) : PositionerSGammaExactVelocityAjustedDisplacementGet
        Return adjusted displacement to get exact velocity
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double DesiredDisplacement

        0
        double * AdjustedDisplacement

        double * AdjustedDisplacement

        Example Command Execution:
            PositionerSGammaExactVelocityAjustedDisplacementGet(GROUP1,0,double  *)
        Example Expected Response:
            -18,PositionerSGammaExactVelocityAjustedDisplacementGet(GROUP1,0,double  *)
        """
        self.send("PositionerSGammaExactVelocityAjustedDisplacementGet("+
                  PositionerName+","+DesiredDisplacement+",double  *)")


    def PositionerSGammaParametersGet(self, PositionerName: str):
        """Function argument(s) : PositionerSGammaParametersGet
        Read dynamic parameters for one axe of a group for a future displacement
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* Velocity

        double* Velocity
        double* Acceleration

        double* Acceleration
        double* MinimumTjerkTime

        double* MinimumTjerkTime
        double* MaximumTjerkTime

        double* MaximumTjerkTime

        Example Command Execution:
            PositionerSGammaParametersGet(GROUP1,double *,double *,double *,double *)
        Example Expected Response:
            ?
        """
        self.send("PositionerSGammaParametersGet("+PositionerName+
                  ",double *,double *,double *,double *)")


    def PositionerSGammaParametersSet(self, PositionerName: str,
                                      Velocity: float,
                                      Acceleration: float,
                                      MinimumTjerkTime: float,
                                      MaximumTjerkTime: float):
        """Function argument(s) : PositionerSGammaParametersSet
        Update dynamic parameters for one axe of a group for a future displacement
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double Velocity

        0
        double Acceleration

        0
        double MinimumTjerkTime

        0
        double MaximumTjerkTime

        0


        Example Command Execution:
            PositionerSGammaParametersSet(GROUP1,0,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerSGammaParametersSet("+PositionerName+","+
                  str(Velocity)+","+str(Acceleration)+","+
                  str(MinimumTjerkTime)+","+str(MaximumTjerkTime)+")")


    def PositionerSGammaPreviousMotionTimesGet(self, PositionerName: str):
        """Function argument(s) : PositionerSGammaPreviousMotionTimesGet
        Read SettingTime and SettlingTime
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        double* SettingTime

        double* SettingTime
        double* SettlingTime

        double* SettlingTime


        Example Command Execution:
            PositionerSGammaPreviousMotionTimesGet(GROUP1,double *,double *)
        Example Expected Response:
            -18,PositionerSGammaPreviousMotionTimesGet(GROUP1,double *,double *)
        """
        self.send("PositionerSGammaPreviousMotionTimesGet("+PositionerName+",double *,double *)")


    def PositionerStageParameterGet(self, PositionerName: str, ParameterName: str):
        """Function argument(s) : PositionerStageParameterGet
        Return the stage parameter
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char ParameterName[251]

        0
        char ParameterValue[]

        char ParameterValue[]

        Example Command Execution:
            PositionerStageParameterGet(GROUP1,0,char *)
        Example Expected Response:
            -24,PositionerStageParameterGet(GROUP1,0,char *)
        """
        self.send("PositionerStageParameterGet("+PositionerName+","+ParameterName+",char *)")


    def PositionerStageParameterSet(self, PositionerName: str,
                                    ParameterName: str, ParameterValue: str):
        """Function argument(s) : PositionerStageParameterSet
        Save the stage parameter
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char ParameterName[251]

        0
        char ParameterValue[251]

        0

        Example Command Execution:
            PositionerStageParameterSet(GROUP1,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerStageParameterSet("+PositionerName+","+
                  ParameterName+","+ParameterValue+")")


    def PositionerTimeFlasherDisable(self, PositionerName: str):
        """Function argument(s) : PositionerTimeFlasherDisable
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Disable time flasher
        char PositionerName[251]
        Example Command Execution:
            PositionerTimeFlasherDisable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("PositionerTimeFlasherDisable("+PositionerName+")")


    def PositionerTimeFlasherEnable(self, PositionerName: str):
        """Function argument(s) : PositionerTimeFlasherEnable
        Enable time flasher
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        Example Command Execution:
            PositionerTimeFlasherEnable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("PositionerTimeFlasherEnable("+PositionerName+")")


    def PositionerTimeFlasherGet(self, PositionerName: str):
        """Function argument(s) : PositionerTimeFlasherGet
        Read time flasher parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* MinimumPosition

        double* MinimumPosition
        double* MaximumPosition

        double* MaximumPosition
        double* PositionStep

        double* PositionStep
        bool * EnableState

        bool * EnableState

        Example Command Execution:
            PositionerTimeFlasherGet(GROUP1,double *,double *,double *,bool  *)
        Example Expected Response:
            -18,PositionerTimeFlasherGet(GROUP1,double *,double *,double *,bool  *)
        """
        self.send("PositionerTimeFlasherGet("+PositionerName+",double *,double *,double *,bool  *)")


    def PositionerTimeFlasherSet(self, PositionerName: str,
                                 MinimumPosition: float,
                                 MaximumPosition: float,
                                 TimeInterval: float):
        """Function argument(s) : PositionerTimeFlasherSet
        Set time flasher parameters
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double MinimumPosition

        0
        double MaximumPosition

        0
        double TimeInterval

        0

        Example Command Execution:
            PositionerTimeFlasherSet(GROUP1,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerTimeFlasherSet("+PositionerName+","+
                  str(MinimumPosition)+","+str(MaximumPosition)+","+
                  str(TimeInterval)+")")


    def PositionerUserTravelLimitsGet(self, PositionerName: str):
        """Function argument(s) : PositionerUserTravelLimitsGet
        Read UserMinimumTarget and UserMaximumTarget
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double* UserMinimumTarget

        double* UserMinimumTarget
        double* UserMaximumTarget

        double* UserMaximumTarget

        Example Command Execution:
            PositionerUserTravelLimitsGet(GROUP1,double *,double *)
        Example Expected Response:
            ?
        """
        self.send("PositionerUserTravelLimitsGet("+PositionerName+",double *,double *)")


    def PositionerUserTravelLimitsSet(self, PositionerName: str,
                                      UserMinimumTarget: float,
                                      UserMaximumTarget: float):
        """Function argument(s) : PositionerUserTravelLimitsSet
        Update UserMinimumTarget and UserMaximumTarget
        char PositionerName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        double UserMinimumTarget

        0
        double UserMaximumTarget

        0

        Example Command Execution:
            PositionerUserTravelLimitsSet(GROUP1,0,0)
        Example Expected Response:
            ?
        """
        self.send("PositionerUserTravelLimitsSet("+PositionerName+","+
                  str(UserMinimumTarget)+","+str(UserMaximumTarget)+")")


    def Reboot(self):
        """Function argument(s) : Reboot
        Reboot the controller

        Example Command Execution:
            Reboot()
        Example Expected Response:
            ?
        """
        self.send("Reboot()")


    def SingleAxisSlaveModeDisable(self, GroupName: str):
        """Function argument(s) : SingleAxisSlaveModeDisable
        Disable the slave mode
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER

        Example Command Execution:
            SingleAxisSlaveModeDisable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("SingleAxisSlaveModeDisable("+GroupName+")")


    def SingleAxisSlaveModeEnable(self, GroupName: str):
        """Function argument(s) : SingleAxisSlaveModeEnable
        Enable the slave mode
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER


        Example Command Execution:
            SingleAxisSlaveModeEnable(GROUP1)
        Example Expected Response:
            ?
        """
        self.send("SingleAxisSlaveModeEnable("+GroupName+")")


    def SingleAxisSlaveParametersGet(self, GroupName: str):
        """Function argument(s) : SingleAxisSlaveParametersGet
        Get slave parameters
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char PositionerName[]

        char PositionerName[]
        double *Ratio

        double *Ratio


        Example Command Execution:
            SingleAxisSlaveParametersGet(GROUP1,char *,double  *)
        Example Expected Response:
            -22,SingleAxisSlaveParametersGet(GROUP1,char *,double  *)
        """
        self.send("SingleAxisSlaveParametersGet("+GroupName+",char *,double  *)")


    def SingleAxisSlaveParametersSet(self, GroupName: str, PositionerName: str,
                                     Ratio: float):
        """Function argument(s) : SingleAxisSlaveParametersSet
        Set slave parameters
        char GroupName[251]
            GROUP1
            GROUP2
            GROUP3
            GROUP4
            GROUP5
            GROUP6
            GROUP1.POSITIONER
            GROUP2.POSITIONER
            GROUP3.POSITIONER
            GROUP4.POSITIONER
            GROUP5.POSITIONER
            GROUP6.POSITIONER
        char PositionerName[251]

        double Ratio

        0


        Example Command Execution:
            SingleAxisSlaveParametersSet(GROUP1,GROUP1,0)
        Example Expected Response:
            ?
        """
        self.send("SingleAxisSlaveParametersSet("+GroupName+","+PositionerName+","+str(Ratio)+")")


    def TCLScriptExecuteAndWait(self, TCLFileName: str, TaskName: str,
                                InputParametersList: str):
        """Function argument(s) : TCLScriptExecuteAndWait
        Execute a TCL script from a TCL file and wait the end of execution to return
        char TCLFileName[256]

        char TaskName[251]

        0
        char InputParametersList[251]

        0
        char OutputParametersList[]

        char OutputParametersList[]


        Example Command Execution:
            TCLScriptExecuteAndWait(TestVersion.tcl,0,0,char *)
        Example Expected Response:
            ?
        """
        self.send("TCLScriptExecuteAndWait("+TCLFileName+","+TaskName+","+
                  InputParametersList+",char *)")


    def TCLScriptExecute(self, TCLFileName: str, TaskName: str,
                         ParametersList: str):
        """Function argument(s) : TCLScriptExecute
        Execute a TCL script from a TCL file
        char TCLFileName[256]

        char TaskName[251]

        0
        char ParametersList[251]

        0


        Example Command Execution:
            TCLScriptExecute(TestVersion.tcl,0,0)
        Example Expected Response:
            ?
        """
        self.send("TCLScriptExecute("+TCLFileName+","+TaskName+","+ParametersList+")")


    def TCLScriptExecuteWithPriority(self, TCLFileName: str, TaskName: str,
                                     TaskPriorityLevel: str,
                                     ParametersList: str):
        """Function argument(s) : TCLScriptExecuteWithPriority
        Execute a TCL script with defined priority
        char TCLFileName[256]

        char TaskName[251]

        0
        char TaskPriorityLevel[251]

        0
        char ParametersList[251]

        0


        Example Command Execution:
            TCLScriptExecuteWithPriority(TestVersion.tcl,0,0,0)
        Example Expected Response:
            ?
        """
        self.send("TCLScriptExecuteWithPriority("+TCLFileName+","+TaskName+","+
                  TaskPriorityLevel+","+ParametersList+")")


    def TCLScriptKill(self, TaskName: str):
        """Function argument(s) : TCLScriptKill
        Kill TCL Task
        char TaskName[251]

        0


        Example Command Execution:
            TCLScriptKill(0)
        Example Expected Response:
            ?
        """
        self.send("TCLScriptKill("+TaskName+")")


    def TimerGet(self, TimerName: str):
        """Function argument(s) : TimerGet
        Get a timer
        char TimerName[251]

        int* FrequencyTicks

        int* FrequencyTicks


        Example Command Execution:
            TimerGet(Timer1,int *)
        Example Expected Response:
            ?
        """
        self.send("TimerGet("+TimerName+",int *)")


    def TimerSet(self, TimerName: str, FrequencyTicks: int):
        """Function argument(s) : TimerSet
        Set a timer
        char TimerName[251]

        int FrequencyTicks
        0


        Example Command Execution:
            TimerSet(Timer1,0)
        Example Expected Response:
            ?
        """
        self.send("TimerSet("+TimerName+","+str(FrequencyTicks)+")")
