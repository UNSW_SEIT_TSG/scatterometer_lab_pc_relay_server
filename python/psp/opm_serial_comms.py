class OPMSerialComms:
    def __init__(self, ser):
        self.ser = ser
        self.ser.baudrate = 9600

    def request_DSCNT_A_and_get_number_from_power_meter(self):
        self.ser.write('DSCNT_A?\n'.encode()) # Data store count query
        a = self.ser.readline()
        # print(a, int(a))
        return int(a)

    def wait_for_century(self):
        safetyCounter = 0
        safetyLimit = 1000
        while True and safetyCounter < safetyLimit:
            z = self.request_DSCNT_A_and_get_number_from_power_meter()
            print("["+"#"*int(z/5)+" "*(20-int(z/5))+"] Data Collection Progress\r" if z % 5 == 0 else "", end="")
            if z >= 100:
                print()
                break
            safetyCounter += 1

    def initialise_meter_for_readings(self, wavelength:str):
        self.ser.write('DSE_A 1\n'.encode()) # Enable channel A
        self.ser.write('DSSIZE_A 100\n'.encode()) # Data store buffer size is 100.
        self.ser.write('DSBUF_A 0\n'.encode())
        self.ser.write('LLO 0\n'.encode()) # Local keypad
        self.ser.write(('LAMBDA_A ' + wavelength +'\n').encode())

    def get_reading(self, np, verbose_print_results):
        arr = np.array([])
        raw_data_values = []
        z_sum = 0
        for z in range(100):
            z_str = str(z+1)
            self.ser.write(('ds_a? '+z_str+'\n').encode())
            raw_data_line = self.ser.readline()
            #todo, if there is a b1, b2, b3 or b4 status, retry
                        
            data_as_cleaned_string = str(raw_data_line).replace("b'0,", "").replace("b'1,", "").replace("b'4,", "").replace("\\n", "").replace("E", "e").replace("+", "").replace(" ", "").replace("'", "")
            data_value_as_float = float(data_as_cleaned_string)
            raw_data_values.append(data_value_as_float)
            if verbose_print_results:
                print(z_str, raw_data_line, data_as_cleaned_string, data_value_as_float)
            z_sum+=data_value_as_float
        # print(arr)
        # print(raw_data_values)
        arr = np.asarray(raw_data_values)
        # print(arr)
        return float(np.mean(arr, axis=0)), float(np.std(arr, axis=0))
    
    def close(self):
        self.ser.close()