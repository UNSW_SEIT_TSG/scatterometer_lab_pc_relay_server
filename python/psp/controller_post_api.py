"""Collection of commands that can be sent to the XPS-C8.
Commands are sent via POST."""

import requests
class Controller_POST_API:
    """An API class to facilitate simpler communication with controller."""
    def __init__(self, ip_address="131.236.52.97"):
        self.url = "http://" + ip_address + "/cgi/post.cgi"

    def execute_command_with_timeout(self, command_data):
        """Executes a generic command by sending it to the XPS-C8 via POST with
        a 5 second timeout.
        """
        try:
            response = requests.post(self.url, data=command_data, timeout=5)
            with open('payload' + '.html', 'w+') as f:
                f.write(response.text)
            f.close()
        except Exception:
            print("There was an exception")

    def execute_command(self, command_data):
        """This method executes a generic command by sending it to the XPS-C8
        via POST.
        """
        try:
            response = requests.post(self.url, data=command_data)
            with open('payload' + '.html', 'w+') as f:
                f.write(response.text)
            f.close()
        except Exception:
            print("There was an exception")

    def login(self):
        """Sends login request to XPS-C8."""
        self.execute_command({
            "step": "1",
            "acces": "direct",
            "validateacces": "VALID",
            "name": "Administrator",
            "password": "Administrator",
            "nature": "A"
        })

    def reboot(self):
        """Directs XPS-C8 to reboot"""
        self.execute_command_with_timeout({
            "step": "31",
            "numoption": "46",
            "typaction": "execute",
            "executed": "0",
            "apicalled": "Reboot()",
            "cmd": "",
            "apitocall": "Reboot()",
            "receivedbox": ""
        })

    def group_initialise(self, group_name):
        """Directs a group to initialise in preparation for motion."""
        self.execute_command({
            "step": "31",
            "numoption": "46",
            "typaction": "execute",
            "executed": "0",
            "apicalled": "GroupInitialize(" + group_name + ")",
            "cmd": "",
            "apitocall": "GroupInitialize(" + group_name + ")",
            "receivedbox": "0,"
        })

    def group_home_search(self, group_name):
        """Directs a group to find and return to the home position
        (in preparation for motion).
        """
        self.execute_command({
            "step": "31",
            "numoption": "46",
            "typaction": "execute",
            "executed": "0",
            "apicalled": "GroupHomeSearch(" + group_name + ")",
            "cmd": "",
            "apitocall": "GroupHomeSearch(" + group_name + ")",
            "receivedbox": ""
        })

    def group_move_relative(self, group_name, angle="10"):
        """Directs a group motor to move by a specified angular displacement."""
        self.execute_command({
            "step": "31",
            "numoption": "46",
            "typaction": "execute",
            "executed": "0",
            "apicalled": "GroupMoveRelative(" + group_name + "," + angle + ")",
            "cmd": "",
            "apitocall": "GroupMoveRelative(" + group_name + "," + angle + ")",
            "receivedbox": ""
        })

    def group_move_absolute(self, group_name, angle="10"):
        """Directs a group motor to move to a specified angle
        (measured relative to home position).
        """
        self.execute_command({
            "step": "31",
            "numoption": "46",
            "typaction": "execute",
            "executed": "0",
            "apicalled": "GroupMoveAbsolute(" + group_name + "," + angle + ")",
            "cmd": "",
            "apitocall": "GroupMoveAbsolute(" + group_name + "," + angle + ")",
            "receivedbox": ""
        })
    def group_position_current_get(self, group_name):
        """Request the current position given a group_name."""
        self.execute_command({
            "step": "31",
            "numoption": "46",
            "typaction": "execute",
            "executed": "0",
            "apicalled": "GroupPositionCurrentGet("+group_name+",double *)"
        })
