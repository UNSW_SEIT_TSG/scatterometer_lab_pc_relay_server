import time 
import serial 
import datetime 
import numpy as np 
import os 
 
# Import Classes 
from opm_serial_comms        import OPMSerialComms 
from controller_post_api import Controller_POST_API 
# from mono_GPIB_comms     import MonoGPIBComms

class PSQ:
    def __init__(self,opm_serial_comms):
        self.optical_readings      = []
        self.opm_serial_comms          = opm_serial_comms

    def collectPowerMeterReading(self): 
        self.opm_serial_comms.ser.write('DSCLR_A\n'.encode()) 
        self.opm_serial_comms.ser.write('DSE_A 1\n'.encode()) 
        self.opm_serial_comms.wait_for_century() 
        self.optical_readings.append(self.opm_serial_comms.get_reading(np, False)) 

class PSP:
    def __init__(
        self,
        group_name,
        ip_address,
        stage_positions,
        optical_readings,
        opm_serial_comms,
        verbose_print_results,
        current_positionInit,
        final_position,
        motion_step,
    ):
        self.group_name            = group_name
        self.ip_address            = ip_address
        self.stage_positions       = stage_positions
        self.optical_readings      = optical_readings
        self.opm_serial_comms          = opm_serial_comms
        self.verbose_print_results = verbose_print_results
        self.current_positionInit  = current_positionInit
        self.final_position        = final_position
        self.motion_step           = motion_step

        self.ktr                   = self.initialiseActuator(ip_address, current_positionInit)
        self.reading_count         = int(abs((self.final_position-self.current_positionInit)/self.motion_step)+1)
        print("reading count is "+str(self.reading_count))

    def initialiseActuator(self, ip_address_string, current_position): 
        ktr = Controller_POST_API(ip_address_string) 
        ktr.login() 
        time.sleep(2)   # Wait for login to complete 
        ktr.group_initialise(self.group_name) 
        time.sleep(1)   # Allow time for group initialise command 
        ktr.group_home_search(self.group_name) 
        time.sleep(1) 
        ktr.group_move_absolute(self.group_name, str(current_position)) 
        time.sleep(1) 
        return ktr 
        
    def moveActuator(self, current_position): 
        print("moving to: "+str(current_position)) 
        self.ktr.group_move_absolute(self.group_name, str(current_position)) 

    def collectNReadings_initialActions(self, current_position, message_string="press enter when system is ready to take next reading."): 
        self.moveActuator(current_position) 
        time.sleep(5) #jjk 
        #if not message_string in ("press enter when system is ready to take next reading."): #jjk 
        #    in_string = input(message_string) 
        self.collectPowerMeterReading() 
        self.stage_positions.append(current_position) 
        in_string="\n" 
        return in_string 

    def collectPowerMeterReading(self): 
        self.opm_serial_comms.ser.write('DSCLR_A\n'.encode()) 
        self.opm_serial_comms.ser.write('DSE_A 1\n'.encode()) 
        self.opm_serial_comms.wait_for_century() 
        self.optical_readings.append(self.opm_serial_comms.get_reading(np, self.verbose_print_results)) 

    def collectNReadings(self, reading_count, current_position, final_position): 
        in_string = self.collectNReadings_initialActions(current_position,"press enter to start first reading.") 
        for z in range(reading_count-1): 
            current_position += self.motion_step 
            in_string = self.collectNReadings_initialActions(current_position) 

            if in_string.lower() in ('q', 'exit', 'quit'): 
                print("exiting reading cycle") 
                break 
        return z, current_position 

    def writeReadingsToFile(self, waveLength, fileName, experiment_desc): 
        with open(fileName + ".txt", 'a') as out: 
            out.write('\ndescription:\t'+experiment_desc+'\n') 
            out.write('\nwavelength:\t'+waveLength+'\n') 
            out.write("Start Data Timestamp: {:%Y-%b-%d %H:%M:%S}".format(datetime.datetime.now())+"\n") 
            out.write("Total readings being taken, N = "+str(self.reading_count)+"\n") 
            out.write("nth reading: n\n") 
            out.write("stage_position: s\n") 
            out.write("optical_reading: o\n") 
            out.write("n"+"\t"+"s"+"\t"+"o" + "\n") 
            for z in range(len(self.optical_readings)): 
                out.write(str(z)+"\t"+str(self.stage_positions[z])+"\t"+ str(self.optical_readings[z])+'\n') 
            out.write( 
                "End Data Timestamp: {:%Y-%b-%d %H:%M:%S}\n".format(datetime.datetime.now()) + '\n' 
            ) 

def main(mode):
    if mode == "kurtz":
        scs                  = OPMSerialComms(serial.Serial('com4'))
        psp = PSP(
            group_name="GROUP2",
            ip_address="131.236.52.97",
            stage_positions=[],
            optical_readings=[],
            opm_serial_comms=scs,
            verbose_print_results=False,
            current_positionInit = -110,
            final_position       = -200,
            motion_step          = -2 # Amount by which the motor/stage increments, typically "mm" or "°"    
        )
        
        # gpibcs=MonoGPIBComms('GPIB0::4::INSTR')
        # Main line start

        wavelengthList = \
            input("Please specify the set of wavelengths for the readings, comma separated: ") \
            .split(",")

        fileName = \
            os.path.join(
                "..", 
                "readings", 
                input("Please specify the file name. '.txt' will be appended automatically: ")
            )
        
        #Collect description of experiment for annotation of readings.txt 
        experiment_desc=input("Please type a description for the readings: ") 
        
        time.sleep(int(input("Please specify the delay in seconds to wait to begin: "))) 
        
        # take a shutter shut calibration point 
        #print("Taking shutter shut calibration data") 
        #gpibcs.set_shutter(True) 
        time.sleep(2) 
        for waveLength in wavelengthList: 
            scs.initialise_meter_for_readings(waveLength) 
            psp.collectPowerMeterReading() 
        with open(fileName + "_ShutterClosed.txt", 'a') as out: 
            out.write("Shutter closed optical readings\n") 
            for z in range(len(psp.optical_readings)): 
                out.write(wavelengthList[z] + "\t" + str(psp.optical_readings[z]) + '\n') 
            out.write( 
                "End Data Timestamp: {:%Y-%b-%d %H:%M:%S}\n".format(datetime.datetime.now()) + '\n' 
                    ) 
        #gpibcs.set_shutter(False) 
        time.sleep(2) 
        for waveLength in wavelengthList: 
            psp.optical_readings=[] 
            current_position=psp.current_positionInit 
            # print(gpibcs.set_wavelength(waveLength + '.000')) 
            # gpibcs.set_shutter(False) 
            scs.initialise_meter_for_readings(waveLength) 
            readings_taken, current_position=psp.collectNReadings(psp.reading_count, current_position, psp.final_position) 
            psp.writeReadingsToFile(waveLength, fileName, experiment_desc)
        #gpibcs.close() 
        scs.close()
    elif mode == "forbes":
        scs = OPMSerialComms(serial.Serial('com7'))
        psp = PSP(opm_serial_comms=scs)
        psp.optical_readings=[] 
        scs.initialise_meter_for_readings("450")
        psp.collectPowerMeterReading()
        print(psp.optical_readings)
        scs.close()


if __name__ == '__main__':
    # main("kurtz")
    main("forbes")