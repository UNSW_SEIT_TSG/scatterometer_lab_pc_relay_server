"""
Editable parameters for the relay server application.
When editing this file, please only alter values & comments.
"""
ADDRESS_PORT = {
    "gui_to_relay": ('', 5003),
    "relay_to_gui": ('', 5002),
    "xpsc8": ("131.236.52.97", 5001)
}

GROUP_CUSTOM_HOME_POSITIONS = { # This needs to me manually synchronised with client config file.
    "GROUP1": 0.0,
    "GROUP2": 160.0,
    "GROUP3": 0.0,
    "GROUP4": 50.0,
    "GROUP5": 87.9,
    "GROUP6": 30.0,
    "GROUP7": 5.0
}

GROUP_MINIMUM_TARGET_POSITIONS = {
    "GROUP1": -170,
    "GROUP2": -170,
    "GROUP3": -125,
    "GROUP4": 0,
    "GROUP5": -170,
    "GROUP6": -8,
    "GROUP7": -170
}

GROUP_MAXIMUM_TARGET_POSITIONS = {
    "GROUP1": 60,
    "GROUP2": 170,
    "GROUP3": 125,
    "GROUP4": 300,
    "GROUP5": 170,
    "GROUP6": 92,
    "GROUP7": 170
}

AVAILABLE_GROUPS = {
    # "GROUP1", # Disabled due to persistent following error
    "GROUP2",
    "GROUP3",
    # "GROUP4", # Disabled due to ongoing homing issue.
    "GROUP5",
    "GROUP6",
    "GROUP7",
}

COMMANDS_LIST = [
    '"CloseAllOtherSockets()"',
    '"ControllerMotionKernelTimeLoadGet(double  *,double  *,double  *,double  *)"',
    '"ControllerStatusGet(int  *)"',
    '"ControllerStatusStringGet("+str(ControllerStatusCode)+",char *)"',
    '"DoubleGlobalArrayGet("+str(Number)+",double *)"',
    '"DoubleGlobalArraySet("+str(Number)+","+str(DoubleValue)+")"',
    '"ElapsedTimeGet(double *)"',
    '"ErrorStringGet("+str(ErrorStringGet)+",char *)"',
    '"EventExtendedAllGet(char *)"',
    '"EventExtendedConfigurationActionGet(char *)"',
    '"EventExtendedConfigurationTriggerGet(char *)"',
    '"EventExtendedConfigurationTriggerSet("+ExtendedEventName+","+EventParameter1+","+EventParameter2+","+EventParameter3+","+EventParameter4+")"',
    '"EventExtendedGet("+str(ID)+",char *,char *)"',
    '"EventExtendedRemove("+str(ID)+")"',
    '"EventExtendedStart(int *)"',
    '"EventExtendedWait()"',
    '"FirmwareVersionGet(char *)"',
    '"GatheringConfigurationGet(char *)"',
    '"GatheringConfigurationSet("+Type+")"',
    '"GatheringCurrentNumberGet(int *,int *)"',
    '"GatheringDataAcquire()"',
    '"GatheringDataGet("+str(IndexPoint)+",char *)"',
    '"GatheringDataMultipleLinesGet("+str(IndexPoint)+","+str(NumberOfLines)+",char *)"',
    '"GatheringExternalConfigurationGet(char *)"',
    '"GatheringExternalConfigurationSet("+Type+")"',
    '"GatheringExternalCurrentNumberGet(int *,int *)"',
    '"GatheringExternalDataGet("+str(IndexPoint)+",char *)"',
    '"GatheringExternalStopAndSave()"',
    '"GatheringReset()"',
    '"GatheringRunAppend()"',
    '"GatheringRun("+str(DataNumber)+","+str(Divisor)+")"',
    '"GatheringStop()"',
    '"GatheringStopAndSave()"',
    '"GlobalArrayGet("+str(Number)+",char *)"',
    '"GlobalArraySet("+str(Number)+","+ValueString+")"',
    '"GPIOAnalogGainGet("+GPIOName+",int *)"',
    '"GPIOAnalogGainSet("+GPIOName+","+str(AnalogInputGainValue)+")"',
    '"GPIOAnalogGet("+GPIOName+",double *)"',
    '"GPIOAnalogSet("+GPIOName+","+str(AnalogOutputValue)+")"',
    '"GPIODigitalGet("+GPIOName+",unsigned short *)"',
    '"GPIODigitalSet("+GPIOName+","+str(Mask)+","+str(DigitalOutputValue)+")"',
    '"GroupAccelerationSetpointGet("+GroupName+",double  *)"',
    '"GroupAnalogTrackingModeDisable("+GroupName+")"',
    '"GroupAnalogTrackingModeEnable("+GroupName+","+Type+")"',
    '"GroupCorrectorOutputGet("+GroupName+",double  *)"',
    '"GroupCurrentFollowingErrorGet("+GroupName+",double  *)"',
    '"GroupHomeSearch("+GroupName+")"',
    '"GroupHomeSearchAndRelativeMove("+GroupName+","+str(TargetDisplacement)+")"',
    '"GroupInitialize("+GroupName+")"',
    '"GroupInitializeWithEncoderCalibration("+GroupName+")"',
    '"GroupJogCurrentGet("+GroupName+",double *,double *)"',
    '"GroupJogModeDisable("+GroupName+")"',
    '"GroupJogModeEnable("+GroupName+")"',
    '"GroupJogParametersGet("+GroupName+",double *,double *)"',
    '"GroupJogParametersSet("+GroupName+","+str(Velocity)+","+str(Acceleration)+")"',
    '"GroupKill("+GroupName+")"',
    '"GroupMotionDisable("+GroupName+")"',
    '"GroupMotionEnable("+GroupName+")"',
    '"GroupMoveAbort("+GroupName+")"',
    '"GroupMoveAbsolute("+GroupName+","+str(TargetPosition)+")"',
    '"GroupMoveRelative("+GroupName+","+str(TargetDisplacement)+")"',
    '"GroupPositionCorrectedProfilerGet("+GroupName+","+str(PositionX)+","+str(PositionY)+",double  *,double  *)"',
    '"GroupPositionCurrentGet("+GroupName+",double  *)"',
    '"GroupPositionPCORawEncoderGet("+GroupName+","+str(PositionX)+","+str(PositionY)+",double  *,double  *)"',
    '"GroupPositionSetpointGet("+GroupName+",double  *)"',
    '"GroupPositionTargetGet("+GroupName+",double  *)"',
    '"GroupReferencingActionExecute("+PositionerName+","+ReferencingAction+","+ReferencingSensor+","+str(ReferencingParameter)+")"',
    '"GroupReferencingStart("+GroupName+")"',
    '"GroupReferencingStop("+GroupName+")"',
    '"GroupStatusGet("+GroupName+",int  *)"',
    '"GroupStatusStringGet("+str(GroupStatusCode)+",char *)"',
    '"GroupVelocityCurrentGet("+GroupName+",double  *)"',
    '"HardwareDateAndTimeGet(char *)"',
    '"HardwareDateAndTimeSet(0)"',
    '"KillAll()"',
    '"Login("+Name+","+Password+")"',
    '"PositionerAccelerationAutoScaling("+PositionerName+",double *)"',
    '"PositionerAnalogTrackingPositionParametersGet("+PositionerName+",char *,double *,double *,double *,double *)"',
    '"PositionerAnalogTrackingPositionParametersSet("+PositionerName+","+GPIOName+","+str(Offset)+","+str(Scale)+","+str(Velocity)+","+str(Acceleration)+")"',
    '"PositionerAnalogTrackingVelocityParametersGet("+PositionerName+",char *,double *,double *,double *,int *,double *,double *)"',
    '"PositionerAnalogTrackingVelocityParametersSet("+PositionerName+","+GPIOName+","+str(Offset)+","+str(Scale)+","+str(DeadBandThreshold)+","+str(Order)+","+str(Velocity)+","+str(Acceleration)+")"',
    '"PositionerBacklashDisable(GROUP1)"',
    '"PositionerBacklashEnable("+PositionerName+")"',
    '"PositionerBacklashGet("+PositionerName+",double *,char *)"',
    '"PositionerBacklashSet("+PositionerName+","+str(BacklashValue)+")"',
    '"PositionerCorrectorAutoTuning("+PositionerName+","+str(TuningMode)+",double *,double *,double *)"',
    '"PositionerCorrectorNotchFiltersGet("+PositionerName+",double *,double *,double *,double *,double *,double *)"',
    '"PositionerCorrectorNotchFiltersSet("+PositionerName+","+str(NotchFrequency1)+","+str(NotchBandwith1)+","+str(NotchGain1)+","+str(NotchFrequency2)+","+str(NotchBandwith2)+","+str(NotchGain2)+")"',
    '"PositionerCorrectorPIDDualFFVoltageGet(GROUP1,bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)"',
    '"PositionerCorrectorPIDFFAccelerationGet("+PositionerName+",bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)"',
    '"PositionerCorrectorPIDFFAccelerationSet("+PositionerName+","+str(ClosedLoopStatus)+","+str(KP)+","+str(KI)+","+str(KD)+","+str(KS)+","+str(IntegrationTime)+","+str(DerivativeFilterCutOffFrequency)+","+str(GKP)+","+str(GKI)+","+str(GKD)+","+str(KForm)+","+str(FeedForwardGainAcceleration)+")"',
    '"PositionerCorrectorPIDFFVelocityGet("+PositionerName+",bool *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *)"',
    '"PositionerCorrectorPIDFFVelocitySet("+PositionerName+","+str(ClosedLoopStatus)+","+str(KP)+","+str(KI)+","+str(KD)+","+str(KS)+","+str(IntegrationTime)+","+str(DerivativeFilterCutOffFrequency)+","+str(GKP)+","+str(GKI)+","+str(GKD)+","+str(KForm)+","+str(FeedForwardGainVelocity)+")"',
    '"PositionerCorrectorPIPositionGet("+PositionerName+",bool *,double *,double *,double *)"',
    '"PositionerCorrectorPIPositionSet("+PositionerName+","+ClosedLoopStatus+","+KP+","+KI+","+IntegrationTime+")"',
    '"PositionerCorrectorTypeGet("+PositionerName+",char *)"',
    '"PositionerCurrentVelocityAccelerationFiltersGet("+PositionerName+",double  *,double  *)"',
    '"PositionerCurrentVelocityAccelerationFiltersSet("+PositionerName+","+str(CurrentVelocityCutOffFrequency)+","+str(CurrentVelocityCutOffFrequency)+")"',
    '"PositionerDriverFiltersGet("+PositionerName+",double  *,double  *,double  *,double  *,double  *)"',
    '"PositionerDriverFiltersSet("+PositionerName+","+str(KI)+","+str(NotchFrequency)+","+str(NotchBandwidth)+","+str(NotchGain)+","+str(LowpassFrequency)+")"',
    '"PositionerDriverPositionOffsetsGet("+PositionerName+",double  *,double  *)"',
    '"PositionerDriverStatusGet("+PositionerName+",int  *)"',
    '"PositionerDriverStatusStringGet("+str(PositionerDriverStatus)+",char *)"',
    '"PositionerEncoderAmplitudeValuesGet("+PositionerName+",double *,double *,double *,double *)"',
    '"PositionerEncoderCalibrationParametersGet("+PositionerName+",double *,double *,double *,double *)"',
    '"PositionerErrorGet("+PositionerName+",int  *)"',
    '"PositionerErrorRead("+PositionerName+",int  *)"',
    '"PositionerErrorStringGet("+str(PositionerErrorCode)+",char *)"',
    '"PositionerExcitationSignalGet("+PositionerName+",int *,double *,double *,double *)"',
    '"PositionerExcitationSignalSet("+PositionerName+","+str(Mode)+","+str(Frequency)+","+str(Amplitude)+","+str(Time)+")"',
    '"PositionerExternalLatchPositionGet("+PositionerName+",double *)"',
    '"PositionerHardInterpolatorFactorGet("+PositionerName+",int  *)"',
    '"PositionerHardInterpolatorFactorSet("+PositionerName+","+str(InterpolationFactor)+")"',
    '"PositionerHardwareStatusGet("+PositionerName+",int  *)"',
    '"PositionerHardwareStatusStringGet("+str(PositionerHardwareStatus)+",char *)"',
    '"PositionerMaximumVelocityAndAccelerationGet("+PositionerName+",double *,double *)"',
    '"PositionerMotionDoneGet("+PositionerName+",double *,double *,double *,double *,double *)"',
    '"PositionerMotionDoneSet("+PositionerName+","+PositionWindow+","+VelocityWindow+","+CheckingTime+","+MeanPeriod+","+TimeOut+")"',
    '"PositionerPositionCompareAquadBAlwaysEnable("+PositionerName+")"',
    '"PositionerPositionCompareAquadBWindowedGet("+PositionerName+",double *,double *,bool  *)"',
    '"PositionerPositionCompareAquadBWindowedSet("+PositionerName+","+str(MinimumPosition)+","+str(MaximumPosition)+")"',
    '"PositionerPositionCompareDisable("+PositionerName+")"',
    '"PositionerPositionCompareEnable("+PositionerName+")"',
    '"PositionerPositionCompareGet("+PositionerName+",double *,double *,double *,bool  *)"',
    '"PositionerPositionComparePulseParametersGet("+PositionerName+",double *,double *)"',
    '"PositionerPositionComparePulseParametersSet("+PositionerName+","+str(PCOPulseWidth)+","+str(EncoderSettlingTime)+")"',
    '"PositionerPositionCompareSet("+PositionerName+","+str(MinimumPosition)+","+str(MaximumPosition)+","+str(PositionStep)+")"',
    '"PositionerRawEncoderPositionGet("+PositionerName+","+str(UserEncoderPosition)+",double *)"',
    '"PositionersEncoderIndexDifferenceGet("+PositionerName+",double  *)"',
    '"PositionerSGammaExactVelocityAjustedDisplacementGet("+PositionerName+","+DesiredDisplacement+",double  *)"',
    '"PositionerSGammaParametersGet("+PositionerName+",double *,double *,double *,double *)"',
    '"PositionerSGammaParametersSet("+PositionerName+","+str(Velocity)+","+str(Acceleration)+","+str(MinimumTjerkTime)+","+str(MaximumTjerkTime)+")"',
    '"PositionerSGammaPreviousMotionTimesGet("+PositionerName+",double *,double *)"',
    '"PositionerStageParameterGet("+PositionerName+","+ParameterName+",char *)"',
    '"PositionerStageParameterSet("+PositionerName+","+ParameterName+","+ParameterValue+")"',
    '"PositionerTimeFlasherDisable("+PositionerName+")"',
    '"PositionerTimeFlasherEnable("+PositionerName+")"',
    '"PositionerTimeFlasherGet("+PositionerName+",double *,double *,double *,bool  *)"',
    '"PositionerTimeFlasherSet("+PositionerName+","+str(MinimumPosition)+","+str(MaximumPosition)+","+str(TimeInterval)+")"',
    '"PositionerUserTravelLimitsGet("+PositionerName+",double *,double *)"',
    '"PositionerUserTravelLimitsSet("+PositionerName+","+str(UserMinimumTarget)+","+str(UserMaximumTarget)+")"',
    '"Reboot()"',
    '"SingleAxisSlaveModeDisable("+GroupName+")"',
    '"SingleAxisSlaveModeEnable("+GroupName+")"',
    '"SingleAxisSlaveParametersGet("+GroupName+",char *,double  *)"',
    '"SingleAxisSlaveParametersSet("+GroupName+","+PositionerName+","+str(Ratio)+")"',
    '"TCLScriptExecuteAndWait("+TCLFileName+","+TaskName+","+InputParametersList+",char *)"',
    '"TCLScriptExecute("+TCLFileName+","+TaskName+","+ParametersList+")"',
    '"TCLScriptExecuteWithPriority("+TCLFileName+","+TaskName+","+TaskPriorityLevel+","+ParametersList+")"',
    '"TCLScriptKill("+TaskName+")"',
    '"TimerGet("+TimerName+",int *)"',
    '"TimerSet("+TimerName+","+str(FrequencyTicks)+")"',
]

STAGE_NAME_TO_GROUP_NAME = {
    "RVS80CC@XPS-DRV01": "GROUP7",
    "IMS100V@XPS-DRV01": "GROUP6",
    "RV160CCHL@XPS-DRV03": "GROUP2",
    "MTM2500CC.1@XPS-DRV01": "GROUP3",
    "IMS300CCHA@XPS-DRV03": "GROUP4",
    "RV120CCHL@XPS-DRV03": "GROUP5",
    "RV240CCHL@XPS-DRV03": "GROUP1",
}
